\chapter{Implementace knihovny DICOM}
\label{sec:dicom-library-implementation}
Tato kapitola se zabývá návrhem a implementací knihovny pro práci s~DICOM soubory.
V~první části kapitoly jsou stanoveny požadavky, které by měly být během implementace splněny.
V~následující části se kapitola zabývá rozborem rozhraní knihovny třetí strany pro práci s~DICOM soubory.
Poslední část kapitoly popisuje implementaci samotné knihovny.

\section{Požadavky}
V~rámci implementace knihovny pro načítání a ukládání DICOM souborů je definováno několik požadavků.
Definované požadavky by měly umožnit snadnou integraci vytvořené knihovny v~rámci dalších systémů.

\subsubsection{Technologie}
Knihovna by měla být implementována pomocí programovacího jazyka C\# z~důvodu možnosti integrace s~dalšími částmi rentgenového systému SCIOX od společnosti Eledus.
Z~důvodu kompatibility knihovny s~různými implementacemi .NET by výsledná knihovna měla cílit na co nejnižší verzi .NET standardu\footnote{Standard pro harmonizaci API implementací .NET\cite{dotnetstandard}}.
Požadavkem je, aby cílená verze .NET standardu byla podporována alespoň nejnovějšími verzemi následujících implementací .NET: .NET Core \footnote{Multiplatformní implementace .NET \cite{dotnetcore}}, .NET Framework\footnote{Implementace .NET pro operační systém Windows\cite{dotnetframework}}, Mono\footnote{Multiplatformní implementace .NET \cite{mono}}.
Dalším z~požadavků na implementaci je, aby bylo možné vytvořenou knihovnu v~pozdějších fázích vývoje distribuovat jako nuget\footnote{Balíčkovací systém pro .NET \cite{nuget}} balíček.

\subsubsection{Testování}
Z~důvodu budoucího využití knihovny v~produkčním kódu rentgenových zařízení je vyžadováno pokrytí implementované knihovny jednotkovými a integračními testy.
Pro snadnou testovatelnost by měly být všechny části knihovny abstraktní, včetně knihoven třetích stran.
V~případě, že knihovny třetích stran nejsou abstraktní, měly by být při implementaci abstrahovány.

\subsubsection{Využití knihoven třetích stran pro DICOM}
Pro usnadnění práce při tvorbě knihovny by měla být využita některá z~dostupných knihoven pro DICOM.
Knihovna třetí strany by měla být od kódu knihovny odstíněna tak, aby ji bylo snadné nahradit jinou knihovnou v~případě, že vybraná knihovna nebude v~budoucnosti vyhovovat všem požadavkům.
Vybraná knihovna třetí strany musí být vydávána pod některou z~licencí, která bude umožňovat její využití v~komerčním systému.
Distribuce knihovny by měla zajištěna pomocí nuget balíčku pro snadnou integraci a aktualizování v~navrhované knihovně.

\subsubsection{Typ dat a způsob ukládání}
Knihovna by měla být využívána pro ukládání a načítání obrazových dat v~rámci \zkratka{zk:CR}.
\begin{samepage}
Knihovna by měla umět uložit snímky s~následujícími parametry:
\begin{itemize}
	\item Variabilní bitová hloubka až 16 bitů.
	\item Jeden monochromatický kanál.
\end{itemize}
\end{samepage}
V~rámci této práce bude knihovna využita pro rekonstrukci HDR snímku pomocí metody popsané v~kapitole \ref{sec:debevec-hdr}, proto by měla být schopná načíst a pracovat s~DICOM atributem doby expozice.

Ukládání a načítání souboru by mělo být odstíněno tak, aby tato funkcionalita byla nezávislá na souborovém systému operačního systému a zároveň bylo možné načítání ze souborového systému hostitelského operačního systému nahradit načítáním z~jiného zdroje (například pomocí SFTP).

\section{Knihovna třetí strany}
\label{sec:third-party-library}
Jedním z~požadavkům je využití knihovny třetí strany pro načtení atributů DICOM souboru.
Využití knihovny třetí strany usnadní implementaci navrhované knihovny.
Knihovna by měla být použita tak, aby bylo možné ji kdykoliv nahradit jinou knihovnou případně vlastní implementací.
Dalším požadavkem na knihovnu je, aby byla implementovaná v~jazyce C\# a cílila na co nejnižší .NET standard.
Knihovna by měla být vydána pod licencí, která umožňuje její využití v~uzavřeném komerčním systému.

\subsection{Fellow Oak DICOM (FoDicom)}
Jednou z~implementovaných knihoven pro DICOM je knihovna FoDicom.
Tato knihovna je vydávána pod otevřenou licencí MS-PL, které umožňuje její integraci do uzavřeného komerčního systému.
Knihovna je implementována v~programovacím jazyce C\# a cílí na .NET standard 1.3, díky čemuž lze knihovnu využít v~programech implementovaných ve všech dostupných implementacích .NET: .NET Core, .NET Framework, Mono, Universal Windows Platform a Xamarin. Knihovna je distribuována jako nuget balíček pro její snadnou integraci.

\subsubsection{Rozhraní knihovny FoDicom}
Implementovanou funkcionalitu knihovny FoDicom lze využívat pomocí rozhran, které je definováno třídami.
V~rámci implementované knihovny nebude využita všechna funkcionalita knihovny FoDicom, ale pouze její části, kterými jsou:
\begin{itemize}
	\item Načítání a ukládání DICOM souboru.
	\item Práce s~atributy načteného souboru.
	\item Přístup k~DICOM image pixel modulu.
\end{itemize}

\paragraph{Načítání a ukládání souboru}
K~načítání a ukládání souboru slouží metody jedné ze základních tříd \textit{DicomFile}.
Těmito metodami jsou metody \textit{Save} a \textit{Load} a jejich asynchronní ekvivalenty \textit{SaveAsync} a \textit{LoadAsync}.
Všechny z~metod jsou přetížené tak, že lze parametry souboru předat jako: cestu k~souboru, která je vyjádřena textovým řetězcem (string) a bytový stream reprezentující načtený soubor v~paměti (Stream).
Z~těchto metod byly jako vhodné vybrány ty, které umožňují asynchronní volání s~parametrem souboru reprezentovaného bytovým tokem (výpis \ref{list:fodicom-file-methods}).
\begin{listing}[h]
\begin{minted}{csharp}
Task SaveAsync(Stream stream, DicomWriteOptions options = null);
static Task<DicomFile> OpenAsync(Stream stream);
\end{minted}
\caption{Metody FoDicom pro práci se souborem.}
\label{list:fodicom-file-methods}
\end{listing}

\paragraph{Práce s~atributy}
Rozhraní pro práci s~DICOM atributy poskytuje třída \textit{DicomDataset}.
Tato třída zároveň implementuje rozhraní \textit{IEnumerable<DicomItem>}, tudíž lze pomocí instance této třídy iterovat po jednotlivých instancích třídy \textit{DicomItem}, které obsahuje.
Pro práci s~atributy třída poskytuje několik vybraných metod, které lze vidět ve výpisu \ref{list:fodicom-attributes-methods}.
\begin{listing}[h]
\begin{minted}{csharp}
T Get<T>(DicomTag tag, T defaultValue);
bool Contains(DicomTag tag);
DicomDataset AddOrUpdate<T>(DicomTag tag, params T[] values);
DicomDataset AddOrUpdate(IEnumerable<DicomItem> items);
DicomDataset AddOrUpdate(params DicomItem[] items);
DicomDataset Remove(DicomTag dicomTag);
DicomDataset Clone();
\end{minted}
\caption{Metody FoDicom pro práci s~atributy.}
\label{list:fodicom-attributes-methods}
\end{listing}

Získání hodnoty atributu lze provést pomocí metody \textit{Get}, jejíž návratová hodnota je generického datového typu \textit{T}, jehož reprezentace je odvozena od datového typu parametru \textit{defaultValue}.
Parametr \textit{defaultValue} slouží jako defaultní návratová hodnota v~případě, že instance \textit{DicomDataset} neobsahuje žadný atribut se značkou, která je definována parametrem \textit{tag} s~datovým typem \text{DicomTag}.
Pro zjištění, zda daná instance třídy \textit{DicomDataset} obsahuje atribut, slouží metoda \textit{Contains}.
Návratová hodnota této metody značí, zda instance obsahuje atribut se značkou, která je definována parametrem \textit{tag} datového typu \textit{DicomTag}.
Datový typ návratové hodnoty je \textit{bool}.

Metoda \textit{AddOrUpdate} a její přetížení slouží k~přidávání nebo aktualizaci atributů.
Přetížení s~parametrem \textit{IEnumerable<DicomItem>} umožňuje přidat nebo aktualizovat atributy instancí třídy, která implementuje \textit{IEnumerable<DicomItem>}, čili toto přetížení může být využito pro aktualizování atributů atributy, které jsou uloženy v~jiné instanci třídy DicomDataset.
Další z~přetížení umožňuje přidat atributy jako pole parametrů instancí třídy DicomItem.
Poslední z~možností volání \textit{AddOrUpdate}, umožňuje přidat nebo aktualizovat atribut pomocí parametrů, definujících značku atributu s~datovým typem \textit{DicomTag} a hodnotu atributu s~generickým typem \textit{T}.

\paragraph{Práce s~image pixel modulem}
Rozhraní pro práci s~DICOM image pixel modulem poskytuje třída \textit{DicomPixelData}.
Tato třída obsahuje atributy, které jsou reprezentovány členem \textit{Dataset} datového typu \textit{DicomDataset}.
Počet snímků v~atributech je reprezentován členem \textit{NUmberOfFrames} datového typu \textit{int}.
Pro čtení a ukládání snímků jsou dostupné metody \textit{GetFrame} a \textit{AddFrame}, které lze vidět ve výpisu \ref{list:fodicom-pixel-methods}.
\begin{listing}[h]
\begin{minted}{csharp}
public abstract void AddFrame(IByteBuffer data);
public abstract IByteBuffer GetFrame(int frame);
\end{minted}
\caption{Metody FoDicom pro práci s~image pixel modulem.}
\label{list:fodicom-pixel-methods}
\end{listing}
Metoda \textit{AddFrame} bere jako parametr instanci třídy, která implementuje rozhraní \textit{IByteBuffer}, tento parametr reprezentuje byty reprezentující jeden snímek.
Ke čtení snímků z~pixel data modulu třída \textit{DicomPixelData} poskytuje metodu \textit{GetFrame}.
Metoda bere jako parametr pořadí snímku, který je vrácen jako návratová hodnota s~datovým typem \textit{IByteBuffer}.

\section{Implementace knihovny}
\label{sec:dicom-implementation-part}
Pro implementaci knihovny je využita knihovna třetí strany, jejíž rozhraní bylo popsáno v~předešlé kapitole.
Knihovna je vytvořena jako projekt typu Class library.
Takto vytvořený projekt lze snadno distribuovat jako nuget balíček.
Knihovna cílí na .NET standard 2.0.

V~první fázi je vytvořena abstrakce rozhraní knihovny třetí strany kvůli snadnému testování jednotkovými testy.
V~dalších fázích implementace je abstrakce knihovny třetí strany využita tak, aby bylo možné knihovnu třetí strany kdykoliv nahradit jinou knihovnou či vlastní implementací.
Toho je docíleno odstíněním abstrakcí knihovny třetí strany další vrstvou abstrakce, které reprezentuje logické části pro práci s~DICOM soubory.

Pro načítání a ukládání souboru musí být vytvořena abstraktní vrstva abstrahující závislost na souborovém systému operačního systému.
Tato vrstva může být implementována různými třídami, které poskytují bytový tok reprezentující byty souboru.
Graf závislostí implementované knihovny popisuje obrázek \ref{fig:dicom-library-architecture}

\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{obrazky/dicom-library-architecture}
	\caption{Graf závislostí implementované aplikace.}
	\label{fig:dicom-library-architecture}
\end{figure}

Graf závislostí ukazuje závislosti mezi jednotlivými třídami knihovny a rozhraními, které implementují.
Název všech uzlů rozhraní začíná velkým I~(například IDicomModel<T>), všechny ostatní uzly náleží třídám.
Šedé spoje značí závislost mezi dvěma uzly.
Třída uzlu z~kterého šedý spoj vede, je závislá na třídě uzlu, do kterého je spoj veden.
Šedé spoje značí implementace rozhraní, tak, že třída z~jehož uzlu spoj vede, implementuje rozhraní do jehož uzlu spoj vede.

\subsection{Abstrakce rozhraní FoDicom}
V~levé části diagramu \ref{fig:dicom-library-architecture} se nachází rozhraní, které bylo navrženo pro abstrakci knihovny třetí strany.
Abstrakce se skládá ze tří rozhraní \textit{IDicomFileWrapper}, \textit{IDicomDatasetWrapper}, \textit{IDicomPixelDataWrapper} a jejich implementací v~podobě tříd \textit{DicomFileProxy}, \textit{DicomDatasetProxy} a \textit{DicomPixelDataProxy}.

Jednotlivé abstrakce obsahují členy a metody, které byly popsány v~přehledu rozhraní knihovny FoDicom.
Každá z~proxy tříd obsahuje instanci objektu, který má být abstrakcí odstíněn.
Tato instance není definována v~rozhraní, které třída implementuje.
Přítomnost abstrahovaného objektu v~proxy třídě lze využít k~snadnější práci s~abstrakcí při navrhování dalších částí systému, případně při rozšiřování abstrakce o~další členy a metody.

\subsection{Abstrakce knihovny FoDicom}
Mimo abstrahování rozhraní, které poskytuje knihovna FoDicom je odstíněna i knihovna samotná.
Odstínění knihovny znamená vytvoření další úrovně abstrakce nad abstrakcí, která odstiňuje abstrakci rozhraní.
Tato abstrakce je zajištěna rozhraními \textit{IDicomModel<T>}, \textit{IImagePixelModule<T>} a \textit{IDicomFile<T>} a jejich implementace FoDicomModel, FoDicomDicomFile a FoDicomImagePixelModule.
Generický parametr všech rozhraní a jejich implementačních tříd definuje datový typ z~kterého budou brány atributy.

Jediné místo kde uniká závislost skrz abstrakci je využití rozhraní \textit{IDicomDatasetWrapper} v~třídách \textit{FoDicomModel} a \textit{FoDicomFile}.
Tato část rozhraní knihovny FoDicom nebyla dostatečně odstíněna z~důvodu nutnosti abstrahovat další části rozhraní knihovny.
Tyto části knihovny budou odstíněny během budoucího vývoje knihovny.

\subsection{Implementace práce se souborem}
Pro načítání a ukládání DICOM souboru bylo navrženo rozhraní \textit{IFileStreamProvider}.
Rozhraní definuje, že jeho implementace by měly poskytovat soubor v~podobě bytového toku.
K~tomuto rozhraní je vytvořena implementace v~podobě třídy \textit{PhysicalFileStreamProvider}, která umí načítat soubor ze souborového systému a poskytovat jeho bytový tok ostatním částem knihovny skrz definované rozhraní.

Přidáním dalších implementací \textit{IFileStreamProvider} rozhraní by bylo možné DICOM soubory načítat z~dalších zdrojů, jako například SFTP server nebo relační databáze.

\subsection{Implementace modelu a image pixel modulu}
Všechny předešlé části knihovny byly využity k~implementaci modelu DICOM souboru a jeho image pixel modulu pro ukládání snímků.
Jak již bylo zmíněno, tyto dvě části jsou závislé na rozhraní \textit{IIDicomDatasetWrapper}, které by mělo být v~dalších fázích vývoje knihovny nahrazeno další úrovní abstrakce pro úplné odstínění knihovny třetí strany.

Model a image pixel module jsou reprezentovány rozhraními \textit{IDicomModel<T>} a \textit{IImagePixelModule<T>} a jejich implementacemi třídami \textit{FoDicomModel} a \textit{FoDicomImagePixelModule}.
V~případě, že by bylo třeba nahradit knihovnu třetí strany jinou knihovnou, je možné vytvořit nové implementace obou rozhraní.
Díky definovaným rozhraním se ostatní části systému ve kterém bude knihovna používána, nebudou muset upravovat díky odstínění rozhraním.