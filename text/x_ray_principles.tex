\chapter{Principy pořizování rentgenových snímků}
\label{sec:principy}
Pořizování rentgenových snímků je prováděno pomocí zdroje záření, rentgenovaného objektu a detektoru rentgenového záření.
Rentgenové záření je definováno jako elektromagnetické vlny s~vlnovou délkou od \SI{10}{\nano\meter} do \SI{1}{\pico\meter}, jejichž energie se nejčastěji pohybuje od \SI{1}{\kilo\eV} do \SI{200}{\kilo\eV}. \cite{AstroNuklFyzika-JadRadFyzika}

Scéna pro pořizování rentgenových snímků (\cref{fig:xray-scene}) se skládá z výše zmíněných objektů: zdroje rentgenového záření, rentgenovaného objektu a detektoru rentgenového záření.
Zdroj rentgenového záření ozařuje elektromagnetickým vlněním o~vlnové délce od \SI{5}{\pico\meter} do \SI{50}{\pico\meter} rentgenovaný objekt.
V~závislosti na tloušťce a absorpčních vlastnostech objektu se část záření absorbuje a zbylá část záření dopadá na detektor rentgenového záření.
Výstupem detektoru je poté obraz ve stupních šedi. \cite{AstroNuklFyzika-JadRadMetody}
\begin{figure}[bh]
	\includegraphics[width=\textwidth]{xray-scene}
	\caption{Scéna pro pořizování rentgenových snímků.}
	\label{fig:xray-scene}
	\centering
\end{figure}

\section{Vznik rentgenového záření}
\label{sec:vznik-rentgenoveho-zareni}
Elektromagnetické záření, kterému se říká rentgenové, vzniká buď při přechodu elektronů mezi vnitřními vrstvami těžších atomů -- charakteristické X-záření, nebo při dopadu urychlených elektronů na anodu a jejich prudkému zabrzdění -- brzdné záření. \cite{AstroNuklFyzika-JadRadFyzika}

\subsection{Brzdné záření}
V~případě, že se akcelerovaný elektron přiblíží k~jádru atomu (například wolframu), silné Coulombovy síly mezi jádrem atomu a letícím elektronem způsobí zbrzdění elektronu a změnu jeho trajektorie.
Během zpomalení rychle letícího elektronu je jeho kinetická energie přeměňována na brzdné záření. \cite{Diagnostic-Radiology-Physics}
Tento proces popisuje \cref{fig:bremsstrahlung-xray}.

\begin{figure}[bh]
	\centering
	\includegraphics[width=0.75\textwidth]{bremsstrahlung-xray}
	\caption
	[Vznik brzdného záření při interakci rychle se pohybujícího elektronu s~atomem wolframu.]
	{Vznik brzdného záření při interakci rychle se pohybujícího elektronu s~atomem wolframu. \cite{the-xray-beam}}
	\label{fig:bremsstrahlung-xray}
\end{figure}

Ideální spektrum brzdného záření lze popsat pomocí zjednodušeného modelu, který neuvažuje kvantovou mechaniku.
Zjednodušený model pracuje s proudem elektronů přibližující se k~atomu.
Uvážíme-li pole v~okolí jádra atomu rozdělené na několik kruhových vrstev dle působících brzdných sil, tak lze říct, že generované brzdné záření brzděním proudu elektronů má spektrum, které odpovídá plochám těchto vrstev.
Čím blíže je vrstva pole k~jádru atomu, tím větší brzdnou sílou působí na letící elektron a tím větší je energie vygenerovaného fotonu.
Zároveň čím je vrstva pole vzdálenější od jádra atomu, tím větší má plochu a tím více fotonů je schopna vygenerovat.
Fotony generované ve vzdálenějších vrstvách od jádra mají nižší energii vzhledem k~nižším brzdným silám působících na přibližující se elektrony. \cite{The-Physical-Principles-of-Medical-Imaging}

Ideální spektrum brzdného záření zjednodušeného modelu ukazuje \cref{fig:bremsstrahlung-xray-char}.
Ze spektra je zřetelné, že největší energii, která se blíží kinetické energii elektronů má jen zlomek z~celkového počtu generovaných fotonů -- fotony generované elektrony, které byly zabrzděny ve vrstvě nejblíže jádru.

\begin{figure}[bh]
	\centering
	\includegraphics[width=0.75\textwidth]{bremsstrahlung-xray-char}
	\caption
	[Ideální spektrum brzdného záření.]
	{Ideální spektrum brzdného záření. \cite{Diagnostic-Radiology-Physics}}
	\label{fig:bremsstrahlung-xray-char}
\end{figure}

\subsection{Charakteristické záření}
Charakteristické záření vzniká při přechodu elektronu z~vyšší elektronové vrstvy atomu do nižší.
Při přechodu elektron ztrácí energii, která je emitovaná jako foton charakteristického záření. Energie emitovaného fotonu odpovídá rozdílu energií vrstev, mezi kterými elektron přechází. Spektrum charakteristického záření je monochromatické a odvíjí se od druhu atomu.
Proces vzniku charakteristického záření popisuje \cref{fig:characteristic-xray}. \cite{Diagnostic-Radiology-Physics}

\begin{figure}[h]
	\centering
	\includegraphics[width=0.75\textwidth]{characteristic-xray}
	\caption
	[Vznik charakteristického záření v~atomu wolframu při vyražení elektronu z~K-Vrstvy elektronem s~kinetickou energií vyšší, než vazební energie vyraženého elektronu.]
	{Vznik charakteristického záření v~atomu wolframu při vyražení elektronu z~K-Vrstvy elektronem s~kinetickou energií vyšší, než vazební energie vyraženého elektronu. \cite{the-xray-beam}}
	\label{fig:characteristic-xray}
\end{figure}

\section{Rentgenka}
Zdrojem rentgenového záření při pořizování rentgenových snímků je nejčastěji speciální vakuová elektronka (\cref{fig:xray-tube}), která je často nazývána jako rentgenka, rentgenová lampa či rentgenová trubice. \cite{AstroNuklFyzika-JadRadMetody}
Rentgenku si lze představit jako zařízení, které převádí energii elektronů na elektromagnetické záření s~odpovídající energií.
Expozice a spektrum záření může být řízena nastavením parametrů rentgenky jako jsou napětí (\SI{}{\kV}), proud (\SI{}{\mA}) a doba expozice (\SI{}{\s}). \cite{Diagnostic-Radiology-Physics}

\subsection{Principy fungování rentgenky}
Energie je do rentgenky přiváděna proudem elektronů s~potenciální energií odpovídající napětí na vysokonapěťovém zdroji (\SI{1}{\kV} odpovídá \SI{1}{\kilo\eV}), potažmo napětí mezi anodou a katodou rentgenky. 
Během průchodu elektronu rentgenkou dochází k~přeměně jeho potenciální energie na energii kinetickou, která je následně přeměněna na elektromagnetické záření a teplo. 

Kinetická energie elektronu při dopadu na anodu odpovídá napětí na vysokonapěťovém zdroji, tedy původní potenciální energii. Kinetická energie je přeměňována zbrzděním atomů při dopadu na anodu při interakci s~atomy materiálu na brzdné a charakteristické rentgenové záření. \cite{The-Physical-Principles-of-Medical-Imaging}
Proud elektronu je emitován při žhavení katody se záporným napětím.
Množství emitovaných elektronů může být řízeno změnou žhavícího proudu. \cite{Diagnostic-Radiology-Physics} 

\subsection{Spektrum rentgenového záření rentgenky}
Jedním z parametrů rentgenky je spektrum vyzařovaného retgenového záření (\cref{fig:xray-spectre}).
Při dopadu elektronů na anodu vznikají dva typy rentgenového záření: brzdné a charakteristické, jejichž obecné principy byly popsány v~kapitole \ref{sec:vznik-rentgenoveho-zareni}.
Obrázek \ref{fig:xray-spectre} ukazuje celkem tři charakteristiky záření při napětí na elektrodách \SI{90}{\kV}:
\begin{enumerate}[label=(\alph*)]
	\item Ideální spektrum brzdného brzdného záření -- spektrum, které již bylo popsáno v~obrázku \ref{fig:bremsstrahlung-xray-char}.
	\item Generované spektrum -- reálné spektrum skládající se z~brzdného a charakteristického záření.
	\item Filtrované spektrum -- spektrum s~útlumem odpovídajícím cloně z hliníku o tloušce \SI{2.5}{\mm}.
\end{enumerate}

\begin{figure}[hb]
	\centering
	\includegraphics[width=\textwidth]{xray-spectre}
	\caption
	[Rentgenka se zdroji proudu a napětí.]
	{Rentgenka se zdroji proudu a napětí. \cite{Diagnostic-Radiology-Physics}}
	\label{fig:xray-spectre}
\end{figure}

\subsection{Konstrukce rentgenky}
Z~důvodu tepelného ohřevu anody po dopadu zrychlených elektronů a vysokého napětí na elektrodách musí být rentgenky oproti běžným elektronkám robustní konstrukce.
Chlazení samotné anody může být zajištěno její velikostí, rotací nebo aktivním chlazením.
Rentgenky lze rozdělit do několika kategorií podle způsobu využití a konstrukce \cite{AstroNuklFyzika-JadRadMetody}:
\begin{itemize}
	\item Rentgenky pro průmyslové ozařování a radioterapeutické použití -- rentgenky s~pevnou anodou, kde je chlazení zajištěno průtokem chladícího média.
	U~toho typu rentgenek je častým požadavkem vysoká energie a intenzita záření.
	Naopak zde není vyžadováno zaměřování elektronů do téměř bodového ohniska. 
	\item Rentgenky pro rentgenovou diagnostiku -- rentgenky se soustředěním elektronů do ohniska. Tento typ využívá rotující anody proti nadměrnému přehřívání anody v~místě ohniska.
	\item Speciální rentgenky - rentgenky rozšířené o~třetí elektrodu (drátěnou mřížku umístěnou mezi katodou a anodou v~těsné blízkosti katody), sloužící k~řízení proudu protékajícího anodou. Proud je řízen napětím, které je přivedeno na drátěnou mřížku.
\end{itemize}

Generované záření ještě předtím, než opustí rentgenku, musí projít skrz různé materiály, které záření filtrují.
Těmito materiály mohou být například samotná anoda, materiál trubice rentgenky, chladicí medium apod.
Úbytek záření při průchodu materiály se uvádí jako útlum, který je ekvivalentní hliníkové cloně o určité tloušťce (\si{\mm}Al).
Typická hodnota útlumu u~běžných rentgenek bývá od \SI{0.5}{\mm}Al do \SI{1}{\mm}Al. 

\begin{figure}[hb]
	\includegraphics[width=\textwidth]{xray-tube}
	\caption
	[Rentgenka se zdroji proudu a napětí.]
	{Rentgenka se zdroji proudu a napětí. \cite{Diagnostic-Radiology-Physics}}
	\label{fig:xray-tube}
	\centering
\end{figure}

\subsubsection{Anoda}
Rentgenové záření je generováno v části rentgenky, která se nazývá anoda.
Tato část bývá tvořena velkým kusem železného materiálu, na který je přivedeno kladné napětí vysokonapěťového zdroje.
Anoda jako taková plní v~rentgence dvě funkce:
\begin{itemize}
	\item Převod elektrické energie na rentgenové záření.
	\item Odvod tepla, které vzniká během procesu generování rentgenového záření.
\end{itemize}
Jako vhodný materiál anody lze považovat materiál takový, který dokáže co největší podíl elektrické energie převést na záření.
Efektivita převodu závisí na atomovém číslu materiálu anody (Z) a kinetické energii dopadajícího elektronu na katodu.\cite{The-Physical-Principles-of-Medical-Imaging}

Nejvíce rentgenek využívá jako materiál anody wolfram s~atomovým číslem 74.
Wolfram je vhodný díky vysokému atomovému číslu a vysokému bodu tání.
V~některých případech je využíváno slitiny wolframu a rhenia, která je však využívána pouze jako povrchový materiál.
Zbylá část anody poté bývá vyrobena z~relativně lehkých materiálů, které mají dobré tepelné vlastnosti, jako třeba molybden nebo grafit.
Výjimkou jsou anody rentgenek pro mamografii, kdy je využíváno molybdenu jako materiálu pro povrch anody, díky vhodnému spektru charakteristického záření.\cite{The-Physical-Principles-of-Medical-Imaging}

Anody lze dělit v~závislosti na výkonu rentgenky (\cref{fig:anodes}).
Pro aplikace, kde není potřebná vysoká energie rentgenového záření, je využíváno stacionární anody.
Tato anoda se skládá z~wolframu, který je usazen v~měděném bloku, jenž slouží k~odvodu tepla.
Tento typ anod se využívá například v dentistických nebo přenosných rentgenkách.
Druhým typem jsou anody rotační, které jsou připojeny k~rotoru motoru, který je umístěn přímo ve vakuové trubici rentgenky.
Vinutí statoru je naopak umístěno vně vakuové trubice. Samotná anoda má kruhový tvar v~podobě terče se skosenými hranami na okrajích.
Paprsek elektronů poté dopadá na zkosenou hranu otáčené anody, tudíž je anoda tepelně namáhána rovnoměrně podél celého obvodu, díky čemuž je možné generovat rentgenové záření s vyšší energií, aniž by byl poškozen povrch anody. \cite{Diagnostic-Radiology-Physics}

\begin{figure}[!h]
	\centering
	\includegraphics[width=0.55\textwidth]{anodes}
	\caption
	[Rotační a stacionární anoda.]
	{Rotační a stacionární anoda. \cite{the-xray-beam}}
	\label{fig:anodes}
\end{figure}


\subsubsection{Katoda}
Část rentgenky, kde je generován proud elektronů pomocí žhavicího vlákna se nazývá katoda (\cref{fig:cathode}).
Katoda je připojena k záporné elektrodě vysokonapěťového zdroje a zároveň k~střídavému zdroji proudu, který slouží k~žhavení vlákna katody a tím i emitování elektronů.\cite{Diagnostic-Radiology-Physics}
Velikost žhavicího vlákna ovlivňuje velikost ohniska proudu elektronů na anodě.
Čím větší žhavicí vlákno je, tím je větší i ohnisko dopadajícího proudu elektronů na anodu.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.75\textwidth]{cathode}
	\caption
	[Katoda s~dvěma žhavicími vlákny.]
	{Katoda s~dvěma žhavicími vlákny. \cite{the-xray-beam}}
	\label{fig:cathode}
\end{figure}

\section{Detekce rentgenového záření}
Rentgenové záření může být detekováno pomocí tzv. detektorů rentgenového záření.
Tyto detektory lze dělit podle způsobu záznamu rentgenového záření na analogové a digitální.
Analogové detektory využívají principu záznamu rentgenového záření v~podobě snímku na film, kdežto digitální zaznamenávají snímky jako digitální informaci.
Digitální detektory lze dále dělit podle toho, zda je snímek digitalizován přímo během procesu ozařování (\zkratka{zk:DR}) nebo až po procesu ozařování (\zkratka{zk:CR}).
Systémy patřící do \zkratka{zk:DR} lze rozdělit dle toho, zda snímají přímo rentgenové záření nebo rentgenové záření převedené na záření světelné \zkratka{zk:DR} s~přímým a nepřímým převodem.

\subsection{Parametry detektorů}
Detektory lze charakterizovat na základě jejich parametrů.
Na základě těchto parametrů lze určit vhodnost využití detektoru pro danou úlohu.

\subsubsection{Velikost pixelu, velikost detektoru a rozlišení}
Jako jedny ze základních parametrů detektorů lze považovat velikost pixelu, velikost detektoru (velikost detekční plochy) a rozlišení detektoru.
Velikost pixelu udává reálnou velikost rentgenovaného objektu, která odpovídá jednomu pixelu snímaného snímku.
Pohybem rentgenovaného předmětu ve scéně  směrem k detektoru je velikost pixelu zvyšována a naopak oddalováním předmětu od detektoru je velikost snižována.
Velikost detektoru ovlivňuje, jak velkou scénu lze snímat (flat-panel detektory), případně jakým způsobem je objekt ve scéně snímán (například snímání pohybujícího se předmětu 
pomocí řádkového CCD detektoru).
Rozlišení digitálních detektorů odpovídá počtu pixelů/snímačů a je udáváno v~počtech pixelů na šířku a výšku.

\subsubsection{Prostorové rozlišení}
Minimální možná vzdálenost mezi dvěma kontrastními objekty, při které lze tyto dva objekty pomocí detektoru rozlišit, se nazývá prostorové rozlišení detektoru. \cite{Advances-in-Digital-Radiography}
Jinými slovy je to minimální možná vzdálenost mezi dvěma objekty, při kterém nám tyto objekty nesplývají v~jeden.
V případě, že digitální snímek není žádným způsobem upraven, lze říct, že velikost pixelu odpovídá prostorovému rozlišení detektoru.
Tedy pokud má pixel velikost $\SI{1}{\milli\meter} \times \SI{1}{\milli\meter}$, prostorové rozlišení detektoru je stejné.
V situaci, kdy je snímku dvakrát sníženo rozlišení pomocí lokálního průměru, velikost pixelu je zdvojnásobena na $\SI{4}{\milli\meter} \times \SI{4}{\milli\meter}$, přičemž prostorové rozlišení detektoru zůstává stejné.

\subsubsection{Modulační přenosová funkce -- Modulation Transfer Function (MTF)}
Degradace kontrastu při převodu rentgenového záření v celém měřícím řetězci detektoru je určena parametrem \zk{zk:MTF}.
Tento parametr úzce souvisí s~prostorovým rozlišením detektoru. 
Zvyšováním frekvence vstupního signálu k hranici prostorového rozlišení hodnota \zk{zk:MTF} klesá k nule, naopak pro stacionární signál se hodnota \zk{zk:MTF} blíží k jedné.
Díky tomu je \zk{zk:MTF} vhodná pro určení reálného prostorového rozlišení detektoru, protože zahrnuje míru rozostření a změny kontrastu v~určitém rozsahu prostorových frekvencí.
\cite{Modulacni-Prenosova-Funkce}

\subsubsection{Dynamický rozsah}
Odezva výstupu detektoru na jeho vstup je nazývána jako dynamický rozsah.
Tento parametr je popsán funkcí dávky rentgenového záření, jejíž výstupem jsou hodnoty v~rozsahu od minimální do maximální možné hodnoty výstupu detektoru (\cref{fig:dynamic-range}).
Například filmové systémy se vyznačují nízkým dynamickým rozsahem, tudíž se může stát, že nesprávně zvolenou expoziční dobou lze snadno snímek přeexponovat.
Naopak u~digitálních detektorů je dynamický rozsah detektoru vysoký, čímž riziko přeexponování snímku klesá.
Závislosti výstupů filmových a obecných digitálních detektorů na dávce rentgenového záření popisuje obrázek \ref{fig:dynamic-range}. \cite{Advances-in-Digital-Radiography}

\begin{figure}[ht]
	\centering
	\includegraphics{dynamic-range}
	\caption[Závislost výstupu detektorů na dávce rentgenového záření.]
	{Závislost výstupu detektorů na dávce rentgenového záření. \cite{Advances-in-Digital-Radiography}}
	\label{fig:dynamic-range}
\end{figure}

\subsubsection{Detekční kvantová účinnost -- Detective Quantum Efficiency (DQE)}
Účinnost detektoru při převodu energie fotonu rentgenového záření na výstupní snímek popisuje parametr DQE. Tento parametr lze definovat jako poměr poměru signál-šum -- signal-to-noise ratio (SNR) výstupního signálu ku SNR vstupního signálu. SNR výstupního a vstupního signálu je funkce závislá na jeho prostorové frekvenci.
Čím vyšší hodnota \zk{zk:DQE} je, tím menší dávka rentgenového záření je potřeba k~získání snímku o~stejné kvalitě oproti detektoru s~nižší hodnotou \zk{zk:DQE}.
Tuto závislost lze také vyjádřit tak, že při stálé době expozice detektor s~vyšší \zk{zk:DQE} detekuje kvalitnější snímek oproti detektoru s~nižší hodnotou \zk{zk:DQE}.
Hodnota \zk{zk:DQE} ideálního detektoru je hodnota 1. V~tomto případě by byl detektor schopen přeměnit veškerou energii rentgenového záření na obrazovou informaci.

\section{Filmové systémy}
Pro získávání snímků pomocí filmu je využíváno filmových systémů.
Tento systém využívá filmu, který je citlivý na světelné záření.
Pro získání rentgenového snímku je třeba vložit film do rentgenového zařízení a poté film vyvolat. Film bývá vložen do plastové kazety, uvnitř které je fosforová vrstva, která přeměňuje rentgenové záření na záření světelné, jež lze detekovat filmem vloženým do kazety.
Výhodou filmových systémů je jejich vysoká citlivost a schopnost nasnímat větší množství informací oproti digitálním systémům. \cite{Diagnostic-Radiology-Physics}
Filmové systémy jsou využívány jak ve zdravotnictví, tak v~průmyslu, nicméně jejich využívání je spíše na ústupu.
Filmové systémy jsou uvedeny pouze z historických důvodů a práce se jimi v následujících kapitolách nebude zabývat.

\section{Digitální systémy}
Čím dál častěji jsou filmové systémy nahrazovány systémy digitálními.
Tyto systémy lze rozdělit do dvou kategorií: systémy pro nepřímou radiografii -- Computed Radiography (\zk{zk:CR}) a systémy pro přímou radiografii -- Direct Radiography  (\zk{zk:DR}). \zk{zk:CR} vychází z~klasických filmových systémů, u~kterých je místo filmu využíváno speciálních fosforových fólií, které jsou po samotném snímání digitalizovány ve speciálním zařízení.

\zk{zk:DR} na rozdíl od \zk{zk:CR} převádí záření na digitální snímek přímo, nicméně samotný proces převodu může být přímý (záření je převáděno snímači na elektrickou veličinu) nebo nepřímý (záření je nejprve převedeno na světelné záření, které je poté převáděno na elektrickou veličinu). \cite{Advances-in-Digital-Radiography}

\subsection{Nepřímá radiografie (\zk{zk:CR})}
Využívá technologií, které jsou velice podobné filmovým systémům. Paměťové fólie jsou tvořeny citlivou vrstvou tvořenou fosforovými krystaly. \cite{Advances-in-Digital-Radiography}. 

Proces ukládání informace a její digitalizaci popisuje \cref{fig:cr}.
Energie dopadajícího záření je absorbována a dočasně (řádově několik hodin) uložena v~paměťové fólii přechodem elektronů atomů fosforových krystalů do vyšších energických hladin (metastabilních).
Digitalizace uložené energie ve fólii poté probíhá v~zařízení, které dokáže pomocí laserového paprsku uvolňovat elektrony z~metastabilních hladin do vodivostního pásu.
Při uvolňování elektronů dochází k~emitování světelného záření, které je poté možné měřit pomocí fotonásobiče. \cite{Advances-in-Digital-Radiography}.
Výhodou tohoto systému je snadná integrace do stávajících filmových systémů.
Nevýhodou může být vysoká časová náročnost vyvolávání snímků a nízké prostorové rozlišení. \cite{Diagnostic-Radiology}

\begin{figure}[ht]
	\centering
	\includegraphics{cr}
	\caption
	[Proces získání digitálního snímku při nepřímé radiografii.]
	{Proces získání digitálního snímku při nepřímé radiografii. \cite{Advances-in-Digital-Radiography}}
	\label{fig:cr}
\end{figure}

\subsection{Přímá radiografie (\zk{zk:DR})}
\zk{zk:DR} je možné dále dělit podle způsobu převodu rentgenového řazení na \zk{zk:DR} s~přímým  převodem a \zk{zk:DR} s~nepřímým převodem.
Při přímém převodu je rentgenové záření snímáno přímo, naopak při nepřímém převodu je záření převáděno zpravidla pomocí scintilační vrstvy na záření světelné, které je poté snímáno fotocitlivými snímači.
Rozdíl mezi přímým a nepřímým převodem rentgenového záření popisuje \cref{fig:direct-indirect-tft}.

\begin{figure}[ht]
	\centering
	\includegraphics{direct-indirect-tft}
	\caption
	[Polovodičový TFT detektor s~nepřímým (A) a přímým (B) převodem rentgenového záření]
	{Polovodičový TFT detektor s~nepřímým (A) a přímým (B) převodem rentgenového záření \cite{Radiation-Detection-and-Measurement}}
	\label{fig:direct-indirect-tft}
\end{figure}

\subsubsection{DR s~nepřímým převodem}
Radiografie, která využívá jevu zvaném scintilace, se nazývá DR s nepřímým převodem. Scintilace je jev, při kterém v některých látkách vznikají světelné záblesky při dopadu ionizujícího záření.
U takovýchto detektorů je využíváno scintilační vrstvy k~převodu rentgenového záření na světelné záření, které je poté snímáno dalšími vrstvami detektoru.
Scintilační vrstvy mohou být jak strukturované, tak nestrukturované.
V~případě nestrukturované scintilační vrstvy generované světelné záření přirozeně dopadá na přilehlé snímače světelného záření, které odpovídají jednotlivým pixelům. Naopak v~případě strukturované scintilační vrstvy světelné záření, díky speciální struktuře vrstvy, dopadá pouze na jeden snímač světelného záření.
Díky tomu je zajištěno lepší prostorové rozlišení detektoru. Rozdíly mezi strukturovanou a nestrukturovanou scintilační vrstvou popisuje \cref{fig:structured-unstructured-scintillator}.

\begin{figure}[ht]
	\centering
	\includegraphics{structured-unstructured-scintillator}
	\caption
	[Rozdíl mezi nestrukturovanou (A) a strukturovanou (B) scintilační vrstvou TFT detektoru s~nepřímým převodem.]
	{Rozdíl mezi nestrukturovanou (A) a strukturovanou (B) scintilační vrstvou TFT detektoru s~nepřímým převodem. \cite{Diagnostic-Radiology}}
	\label{fig:structured-unstructured-scintillator}
\end{figure}

V~případě, že je jako detekční vrstva využita \zk{zk:TFT} matice, musí být pod scintilační vrstvu přidána vrstva detekující světelné záření.
Taková vrstva je nejčastěji tvořena foto-diodovým polem.
Elektrická energie generována foto-diodovým polem je poté ukládána pomocí kondenzátorů \zk{zk:TFT} matice a následně převedena AD převodníkem.

Detekční vrstva může být také realizována pomocí \zkratka{zk:CCD}. V~tomto případě je pomocí \zk{zk:CCD} detektorů přímo detekováno viditelné světlo, které je generováno v~scintilační vrstvě. Díky poměrně malé velikosti \zk{zk:CCD} snímačů je nutné snímače kombinovat do matic, případně využívat čoček.


\subsubsection{DR s~přímým převodem}
Přímého převodu u DR s přímým převodem je docíleno polovodičovou vrstvou vrstvy ze selenu (Se), která dokáže přímo převádět fotony rentgenového záření na elektrickou energii.
\zk{zk:DR} systémy s~přímým převodem mohou využívat buď selenové foto-válce nebo selenové polovodičové vrstvy spojené s~vrstvou tenkovrstvých tranzistorů -- Thin Film Tranzistors (\zk{zk:TFT}). \cite{Advances-in-Digital-Radiography}
Výhodou těchto systémů je především vysoké prostorové rozlišení.
Díky nízkému atomovému číslu selenu je u~těchto detektorů nízká absorpce rentgenového záření.
Díky tomuto faktu je systémů s~přímým převodem využíváno spíše v~mamografii. \cite{Diagnostic-Radiology}

Systémy, které využívají foto-válců, otáčí válcem, na který dopadá rentgenové záření, které se přeměňuje na náboj na povrchu válce. 
Náboj na povrchu otáčejícího se válce odpovídá množství dopadajícího rentgenového záření. \cite{Advances-in-Digital-Radiography}.
Podobného principu je  využíváno u foto válců laserových tiskáren.

Systémy s~vrstvou \zk{zk:TFT} (\cref{fig:direct-tft}) pracují na podobném. Matice \zk{zk:TFT} ukládá generovaných náboj do paměťových kondenzátorů, jejichž napětí může být převedeno AD převodníkem.
Takovýto detektor se skládá z~elektrody na kterou je přivedeno vysoké kladné napětí, polovodičové vrstvy selenu a vrstvy \zk{zk:TFT} kondenzátory pro ukládání náboje generovaného rentgenovým záření.
Rentgenové záření generuje v~selenové vrstvě elektrony a díry.
Díky tomu vzniká v~polovodiči náboj, který je přímo úměrný rentgenovému záření.
Tento náboj je poté díky silnému elektrickému poli odváděn do kondenzátorů v~\zk{zk:TFT} vrstvě. Napětí kondenzátoru může být poté vyčteno přivedením záporného napětí na gate příslušného tranzistoru.

\begin{figure}[ht]
	\centering
	\includegraphics{direct-tft}
	\caption
	[Princip polovodičového TFT detektoru s~přímým převodem rentgenového záření]
	{Princip polovodičového TFT detektoru s~přímým převodem rentgenového záření. \cite{Radiation-Detection-and-Measurement}}
	\label{fig:direct-tft}
\end{figure}
