\chapter{Metody předzpracování rentgenových snímků}
\label{sec:preprocessing-methods}
Následující kapitola se zabývá současnými metodami předzpracování rentgenových snímků.
Jednotlivé části kapitoly popisují teoretické principy metod, které budou využity v~dalších částech práce pro návrh metod odstranění šumu či zvýšení dynamického rozsahu rentgenových snímků.

\section{Reprezentace digitálního snímku}
Pořízený digitální snímek může být reprezentován pomocí signálu v~prostorové nebo frekvenční oblasti.
Způsob reprezentace pořízeného snímku se odvíjí především od způsobu jeho pořízení.
V~případě digitální radiografie jsou snímky pořizovány v~prostorové oblasti.
Snímky ve frekvenční oblasti lze nalézt například u~magnetické rezonance -- Magnetic Resonance Inspection (MRI) nebo \zkratka{zk:CT}.

\subsection{Prostorová oblast}
Oblast, kdy je digitální snímek reprezentován pomocí matice $M \times N$ se nazývá prostorová oblast. Parametr matice $M$ je počet řádků a parametr $N$ je počet sloupců.
Jednotlivé prvky matice se nazývají pixely.
Počet sloupců a řádků určuje rozlišení snímku a je udáváno jako $M \times N$ nebo jako výsledek jejich součinu, čili absolutní počet pixelů -- například rozlišení $1024 \times 1024$ lze zapsat jako \num{1048576} pixelů.
Pixely mohou nabývat hodnot od 0 do $2^{n}$, kde číslo $n$ je nazýváno jako bitová hloubka snímků. V~případě digitálních rentgenových snímků bývá datová hloubka nejčastěji 10, 12, 14 nebo 16 bitů, nicméně hodnota bývá zpravidla ukládána ve 2 bytech, tudíž v~případě bitové hloubky 10, 12 nebo 14 bitů zůstávají vyšší bity nulové.

Digitální snímek v~prostorového oblasti se skládá z~kanálů, které jsou reprezentovány pomocí matic o~stejném počtu řádků a sloupců, jako má výsledný digitální snímek.
Počet kanálů a to, co reprezentují, je určeno barevným modelem.
Například jedním z~nejznámějších barevných modelů je model RGB, který definuje 3 barevné kanály: červený (RED), zelený (GREEN) a modrý (BLUE).
Každý kanál reprezentuje intenzitu jedné z~barev a jejich sečtením vzniká výsledný snímek. Vzhledem k~principu pořizování rentgenových snímků, mají snímky pouze jeden kanál, který zpravidla bývá interpretován pomocí stupňů šedi -- grayscale.

\subsection{Frekvenční oblast}
Frekvence obsažené v~celém snímku jsou reprezentovány pomocí frekvenční oblasti.
Frekvence signálu digitálního snímku odpovídá počtu cyklů na jeden pixel.
Například pokud snímek obsahuje řádek, který se skládá z~periodicky se opakující dvojice černého a bílého pixelu, digitální snímek bude obsahovat signál o~frekvenci \SI{1/2}{\hertz}. 

Vzhledem k~tomu, že ve většině případů je pořízený snímek v~prostorové oblasti, musí být snímek do frekvenční oblasti převeden.
V~případě digitálních snímků bývá pro převod nejčastěji využíváno diskrétní Fourierovy transformace (DFT). 
Pro zpětný převod je poté využíváno zpětné DFT.
DFT a zpětná DFT jsou dle \cite{Image-Processing-Analysis-and-Machine-Vision} definovány jako:
\begin{equation}
\label{eq:dft}
F(u,v)=\sum_{m=0}^{n-1} \sum_{y=0}^{N-1} f(m,n) e^{\frac{-j2\pi um}{M} - \frac{j2\pi vn}{N}}
\end{equation}
a
\begin{equation}
\label{eq:back_dft}
f(m,n)=\frac{1}{MN}\sum_{u=0}^{M-1}\sum_{v=0}^{N-1}F(u,v)e^{\frac{j2\pi um}{M}+\frac{j2\pi vn}{N}},
\end{equation}
kde $f(m,n)$ představuje konkrétní pixel obrázku v~prostorové oblasti s~indexem $m$ a $n$, $N$ a $M$ jsou rozměry snímku. $F(u,v)$ je reprezentace snímku ve frekvenční oblasti pro frekvence $u$ a $v$.
Hodnota $u$ a $v$ se pohybuje v~rozsahu hodnot od 0 do $M-1$ pro $u$ a od 0 do $N-1$ pro $v$.
Hodnota $F(0,0)$ udává velikost stejnosměrné složky obrázku a $F(M-1,N-1)$ udává velikost složky s~nejvyšší možnou frekvencí.
Normalizační člen (\ref{eq:back_dft}) $\frac{1}{MN}$ může být přesunut do (\ref{eq:dft}), kdy pro $F(0,0)$ je poté stejnosměrná složka rovna průměrné hodnotě pixelů snímku.

\section{Metody předzpracování rentgenových snímků}
Operace prováděné při předzpracování digitálních RTG snímků lze dle \cite{baxes} a \cite{Gonzalez} rozdělit do pěti základních kategorií.
Tyto kategorie zahrnují:
\begin{itemize}
\item Obnovení -- rekonstrukce poškozených snímků například vadami snímače.
\item Zvýšení kvality -- operace sloužící pro zvýšení kvality za účelem zvýšení diagnostické hodnoty snímku.
Nejčastěji se jedná o~zvýraznění hran, úpravu kontrastu apod.
\item Syntézu -- sloučení více snímků pro získání více informací.
Sloučením může vzniknout například nový snímek nebo 3D model v~případě \zk{zk:CT}.
\item Kompresi -- snížení velikosti snímků.
\item Analýzu -- operace zahrnující segmentaci, klasifikaci apod.
\end{itemize}

V~případě digitálních rentgenových snímků a metod jejich předzpracování se můžeme setkat s~operacemi ze všech zmíněných kategorií.
Operace pro obnovení se nejčastěji zabývají odstraněním šumu, který vzniká díky nehomogenitě generovaného rentgenového záření.
Operace pro analýzu zahrnují segmentaci obrazu a klasifikaci.
V~případě operací pro zvyšování kvality je v~radiologii využíváno zejména zvýrazňování hran, úpravy kontrastu a dynamického rozsahu.
Kompresní operace jsou využívány při ukládání a archivaci digitálních snímků, kdy je kladen důraz na snížení velikosti digitálních snímků při zachování dostatečné kvality.

\subsection{Odstranění šumu průměrováním}
\label{sec:noise-removement-methods}
Digitální snímky pořízené pomocí běžných metod popsaných v~literární rešerši obsahují šum, který vzniká v~celém řetězci během pořizování snímku.
Šum vzniká díky několika náhodným jevům, které jsou způsobeny:
\begin{itemize}
	\item Počtem fotonů, které opustí zdroj záření (Poissonovo rozdělení).
	\item Počtem fotonů, které projdou nedotčeně ozařovaným objektem. (Binomické rozdělení)
	\item Počtem fotonů zachycených detektorem. (Binomické rozdělení)
	\item Počtem světelných fotonů vygenerovaných ve scintilační vrstvě -- DR s~nepřímým převodem. (Binomické rozdělení)
\end{itemize}

Vzhledem k~tomu, že výběr z~fotonů generovaných procesem s~Poissonovým rozdělení jsou opět fotony v~Poissonově rozdělení, lze říci, že majoritní podíl šumu digitálních snímcích má Poissonovo rozdělení. \cite{X-ray-imaging-noise-and-SNR}
Jednou z~metod pomocí které lze tento šum odstranit, je průměrování.

\subsubsection{Průměrování}
\label{sec:averaging}
Jedna z~metod pro odstranění šumu je průměrování. Tato metoda je velmi citlivá na odchylky vzniklé změnou pozice snímaného objektu ve scéně. Tuto metodu lze popsat pomocí následující rovnice:

\begin{equation}
\label{eq:average}
I_{filtered} \left( x,y \right ) = \frac{1}{N} \sum_{i = 0}^{N - 1}I_{i}\left( x,y \right ),
\end{equation}
kde $x$ a $y$ jsou indexy pixelů, $N$ počet snímků v~množině snímků a $I_{i}$ i-tý snímek z~množiny snímků.

Citlivost této metody na odchylky vzniklé změnou pozice snímaného objektu ve scéně lze řešit pomocí metod založených na \zkratka{zk:AAR}.
Tyto metody spočívají v~předzpracování množiny snímků pomocí registrace obrazových dat a následném průměrování podle (\ref{eq:average}).

\subsubsection{ORB (Oriented FAST and rotated BRIEF)}
Citlivost průměrování na posun snímaného objektu ve scéně lze kompenzovat metodami založených na obrazové registraci.
Jednou z~těchto metod je metoda pro hledání klíčových bodů a jejich deskriptorů využívající \zkratka{zk:FAST} jako detektor klíčových bodů a \zkratka{zk:BRIEF} jako binární deskriptor.

Tato metoda byla navržena jako méně výpočetně náročná alternativa k~metodám \zkratka{zk:SIFT} a \zkratka{zk:SURF}.
Mimo nižší výpočetní náročnost dosahuje metoda \zk{zk:ORB} lepších výsledků vzhledem k~natočení zkoumaného snímku (\cref{fig:orb-comparsion}).
Další výhodou \zk{zk:ORB} oproti metodám \zk{zk:SURF} a \zk{zk:SIFT} je, že je vydána pod otevřenou licencí BSD, čili je možné ji využívat a její modifikace veřejně publikovat. \cite{orb}

Snížení citlivosti metody \zk{zk:BRIEF} na natočení snímku je dosaženo pomocí transformování testovaných bodů do nenatočené souřadnicové soustavy.
Zjištěná orientace zkoumané oblasti natočeného snímku je využita k~transformaci testovaných souřadnic do souřadnicové soustavy odpovídající původní orientaci. Takto upravená metoda BRIEF je dle \cite{brief} označována jako rBRIEF.

\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{orb-comparsion}
	\caption
	[Porovnání úspěšnosti hledání klíčových bodů a jejich deskriptorů pro různé metody v~závislosti na rotaci snímku.]
	{Porovnání úspěšnosti hledání klíčových bodů a jejich deskriptorů pro různé metody v~závislosti na rotaci snímku.\cite{orb}}
	\label{fig:orb-comparsion}
\end{figure}

\subsubsection{BRIEF} 
Pro popis klíčových bodů při obrazové registraci je využíváno deskriptorů.
Jedním z~takových deskriptorů je metoda \zk{zk:BRIEF}, která identifikuje klíčové body snímku pomocí pomocí binárních deskriptorů.
\zk{zk:BRIEF} pracuje na principu testování intenzity náhodně vybraných párů bodů v~konkrétní části snímku, která má velikost $S \times S$. Tento test lze dle \cite{brief} definovat jako:

\begin{equation}
\label{eq:BRIEF-test}
\tau \left (p; x,y \right ):= \left\{\begin{matrix}
1 & pro \quad p\left( x\right ) < p \left(y \right )\\ 
0 & pro \quad p\left( x\right ) \geq  p \left(y \right )\\ 
\end{matrix}\right.,
\end{equation}
kde $\tau$ je výsledek testu, $p\left(x\right)$ a $p\left(y\right)$ jsou intenzity pixelů s~indexy $x$ a $y$ a $p$ je zkoumaná část snímku, na kterou je test prováděn.
Zkoumaná část snímku $p$ by měla být předzpracována vyhlazovacím filtrem.
Výběrem náhodných párů lokací $x$ a $y$ z~části snímku $p$ je vytvořena sada binárních testů, kterou lze definovat jako:

\begin{equation}
\label{eq:BRIEF-test-set}
f_{n_{d}} \left(p \right ) = \sum_{1\leq i\leq n_{d}} 2^{i-1} \tau \left(p; x_i, y_i \right ),
\end{equation}
kde $n_d$ je požadovaná velikost deskriptoru v~počtech bitů.
Z~(\ref{eq:BRIEF-test-set}) plyne, že deskriptor $f_{n_d}$ se bude skládat z~$2^{n_d}$ výsledků testů.
Jak již bylo zmíněno, před aplikací metody BRIEF je nutné zkoumanou část snímku nejprve vyhladit.
V~\cite{brief} je vyhlazení prováděno pomocí integračního vyhlazovacího operátoru o~velikosti okna $5 \times 5$ na zkoumanou část snímku o~velikosti $31 \times 31$ pixelů.
Nevýhodou metody BRIEF je, že si nedokáže dobře poradit s~natočenými snímky.
Proto je BRIEF, jak již bylo zmíněno výše, v~rámci \zk{zk:ORB} rozšířen o~natáčení podle klíčových bodů.

\subsubsection{FAST} 
Základní částí obrazové registrace je nalezení klíčových bodů ve snímku.
Jednou z~metod pro detekci klíčových bodů je metoda \zk{zk:FAST}, která je založena na detekci hran pomocí kruhového okolí.
Algoritmus nejprve vybere pixel $\rho$ o~intenzitě $I_\rho$ u~kterého má být určeno, zda je hranou, či ne.
Následně je zvolen vhodný práh $t$ a kruhové okolí pixelu $\rho$ o~velikosti 16 pixelů.
Pixel je považován za hranu v~případě, že se po obvodu kruhového okolí nachází souvislá množina pixelů, jejichž intenzita je vyšší nebo nižší než intenzita $I_\rho$. Způsob výběru okolí a indexování pixelů popisuje \cref{fig:fast}

Mimo postupu, jenž je popsaný výše, existuje varianta, která se snaží urychlit  celkový proces hledání hran.
Tato metoda spočívá ve zkoumání pouze pixelu číslo 1, 9, 5 a 13. Jestliže jsou pixely 1 a 9 vyhodnoceny jako tmavší nebo světlejší než $I_\rho + t$, pak jsou stejným způsobem otestovány i pixely 5 a 13.
Pixel je považován za hranu v~případě, že alespoň 3 z~množiny pixelů jsou světlejší nebo tmavší než $I_\rho + t$. \cite{rosten_2008_faster}.

V~případě implementace v~\zk{zk:ORB} je využit FAST s~kruhovým okolím 9. Vzhledem k~tomu, že FAST neurčuje orientaci, byla tato metoda v~rámci \zk{zk:ORB} rozšířena o~určení orientace pomocí centroidu. \cite{orb}

\begin{figure}[htb]
	\centering
	\includegraphics{fast}
	\caption
	[Princip výběru okolí a způsob indexování pixelů metody FAST.]
	{Princip výběru okolí a způsob indexování pixelů metody FAST. \cite{rosten_2008_faster}}
	\label{fig:fast}
\end{figure}

\subsubsection{RANSAC (Random sample consensus)} 
K~transformaci snímku z~vychýlené souřadnicové soustavy do původní lze využít metody RANSAC.
Metoda dokáže nalézt model, který definuje vztah mezi dvěma lineárně závislými množinami.
V~případě obrazové registrace se jedná o~množinu párů klíčových bodů se stejnými deskriptory.
Metoda pracuje s~předpokladem, že k~definování lineárního modelu stačí pouze dva body (na rozdíl od metody nejmenších čtverců, která předpokládá, že velké množství dat dokáže eliminovat odchylky).
Algoritmus této metody lze popsat následovně \cite{Image-Processing-Analysis-and-Machine-Vision}:
\begin{enumerate}
	\item Uvažujeme množinu bodů $X = \left\{x_1, x_2, ..., x_n \right\}$ u~které předpokládáme, že odpovídá modelu, který je určen alespoň $m$ body z~množiny. Například pro přímku $m = 2$.
	\item Nastavíme čítač iterací $k$ na hodnotu 1.
	\item Náhodně vybereme $m$ bodů z~množiny a vypočítáme nový model.
	\item Pomocí ztrátové funkce určíme kolik bodů z~množiny odpovídá novému modelu.
	Pokud počet bodů odpovídajících novému modelu překročí práh $t$, model se přepočítá z~těchto bodů.
	\item Zvýšíme čítač iterací o~1 a zkontrolujeme zda $k < K$, kde $K$ je maximální počet iterací.
	Pokud $k < K$ pokračujeme bodem 3, jinak ukončíme algoritmus a jako model zvolíme ten, kterému odpovídalo nejvíce bodů z~množiny.
\end{enumerate}
Takto získaný model lze poté využít jako model pro transformaci souřadnicové soustavy jednoho snímku do souřadnicové soustavy snímku druhého. Jako body, na základě kterých je model vytvářen lze použít množinu párů bodů se stejnými deskriptory získanými pomocí ORB.

\subsection{Zvýšení dynamického rozsahu metodou HDR (High Dynamic Range)}
\label{sec:debevec-hdr}
Uvážíme-li situaci, kdy snímáme scénu a jako výsledek snímání obdržíme dvourozměrné pole změřených hodnot intenzity záření, tak nelze říct, že naměřené hodnoty intenzity přímo odpovídají záření ve scéně. Jinými slovy je nepravděpodobné, že pokud jedna hodnota intenzity je dvojnásobná oproti druhé, tak dopadající záření odpovídající druhé hodnotě je dvojnásobné. Tuto situaci lze sledovat především u~filmových systémů, v~případě systémů DR s~přímým a nepřímým převodem bývá tento jev potlačen -- odezva detektoru je částečně lineární.

Tato nelinearita je dle \cite{Debevec} nazývána nelineární mapování. Vznik nelinearity při převodu záření na digitální snímek závisí především na konstrukci detektoru (scintilační vrstva, typ receptoru apod.). \cite{Debevec}
Odezva detektoru na expozici $X$ bývá označována jako charakteristická křivka detektoru.

Jak u~filmových, tak u~DR systémů se můžeme setkat s~nelinearitou, která je způsobena saturací detektorů, která omezuje dynamický rozsah detektorů. V~případě, že jsou ve snímané scéně zájmové oblasti s~vysokou a nízkou intenzitou záření, není díky nízkému dynamickému rozsahu detektoru možné pozorovat detaily obou oblastí -- jedna z~oblastí je vždy změnou doby expozice, urychlovacího napětí nebo žhavícího proudu posunuta mezi saturační body na charakteristické křivce a druhá oblast do oblasti saturace. Pokud chceme pokrýt celý dynamický rozsah, musíme pořídit sérii snímků s~různými dobami expozice, hodnotami proudu nebo napětí. Metody HDR se zabývají sloučením takto pořízených snímků v~jeden snímek schopný zobrazit scénu s~vysokým dynamickým rozsahem. 
Tyto metody pracují na principu vytvoření mapy popisující intenzitu záření ve scéně, která je pak využita pro škálování hodnot intenzit digitálního snímku. 
Jednou z~těchto metod popsané v~\cite{Debevec} je metoda navržena Paulem E. Debevecem.

Debevecova metoda pracuje na principu sloučení série snímků s~nízkým dynamickým rozsahem v~jeden HDR snímek.
Předpokladem je, že snímky s~nízkým dynamickým rozsahem jsou pořízeny s~rozdílnými expozičními dobami.
Tato metoda byla původně navržena pro získání HDR snímku ze snímků pořízených běžným digitálním fotoaparátem.
Uvážíme-li, že veličiny ve fotometrickém prostoru mají svůj ekvivalent v~radiometrickém prostoru \cite{handbook-of-optics}, je možné princip Debevecovy metody využít i v~případě rentgenových snímků.
V~případě Debevecovy metody se jedná o~veličiny:
\begin{itemize}
	\item Intenzita osvětlení s~jednotkou \si{\lux} a její ekvivalent intenzita záření s~jednotkou \si{\watt\per\metre\squared}
	\item Světelná expozice jednotkou \si{\lux\second} a její ekvivalent fluence energie s~jednotkou \si{\joule\per\metre\squared}
\end{itemize}
Metoda je rozdělena do dvou částí:
\begin{itemize}
	\item získání funkce odezvy detektoru a
	\item sloučení série snímků snímků s~nízkým dynamickým rozsahem v~HDR snímek.
\end{itemize}
\subsubsection{Získání funkce odezvy detektoru} 
Nejdůležitější částí Debevecovy metody je získání funkce odezvy detektoru, díky které lze rekonstruovat reálné ozáření detektoru.
Získání funkce je založeno na principu reciprocity, který říká, že je-li fluence energie $H$ definována jako:
\begin{equation}
\label{eq:debevec_reciprocity}
H = E \Delta t , 
\end{equation}
kde $E$ je intenzita záření a $\Delta t$ doba expozice, pak při dvojnásobném snížení $E$ a dvojnásobném zvýšení $\Delta t$ se nezmění hodnota fluence energie $H_e$.
Charakteristickou křivku převodu fluence energie $H$ na digitální snímek $Z$, lze vyjádřit pomocí funkce $f$ jako:
\begin{equation}
\label{eq:debevec_char}
Z=f\left (H \right ) \Leftrightarrow H = f^{-1}\left ( Z\right )
\end{equation}
Vzhledem k~tomu, že $f$ lze považovat za monotónní funkci, lze fluenci energie $H$ vyjádřit pomocí invertované funkce $f^{-1}(Z)$, jak ukazuje (\ref{eq:debevec_char}).
Po dosazení (\ref{eq:debevec_reciprocity}) do (\ref{eq:debevec_char}) lze výslednou rovnici aplikovat na sérii digitálních snímků jako:
\begin{equation}
\label{eq:debevec_char_ap}
Z_{ij}=f\left (E_i\Delta t_j \right ) \Leftrightarrow f^{-1}\left ( Z_{ij}\right ) = E_i \Delta t_j\quad,
\end{equation}
kde $i$ je index pixelu ve snímku a $j$ index snímku s~určitou dobou expozice. Po aplikování přirozeného logaritmu na obě strany rovnice a zavedení substituce  $g\left( Z_{ij}\right)  = \ln\left( f^{-1} \left( \right) Z_{ij} \right)$ je výsledná rovnice:
\begin{equation}
\label{eq:debevec_char_ap_fin}
g \left( Z_{ij} \right) = \ln \left( E_i \right)  + \ln \left( \Delta t_j \right),
\end{equation}
kde $Z_{ij}$ a $\Delta t_j$ jsou známé hodnoty a $g$ a $E_i$ jsou neznámé, které potřebujeme nalézt. K~nalezení neznámých lze využít metody nejmenších čtverců aplikované na rovnice vycházejících z~(\ref{eq:debevec_char_ap_fin}). Minimalizaci účelové funkce (objective function) $O$ pomocí metody nejmenších čtverců lze definovat jako:
\begin{align}
\label{eq:ls}
O~&= \sum_{i=1}^{N} \sum_{j=1}^{P} \left [g \left(Z_{ij} \right ) -\ln E_i - \ln\Delta t_j \right ]^2 
+ \lambda \sum_{z=Z_{min}+1}^{Z_{max}-1}{g}'' \left(z \right )^2 \\
{g}'' \left(z \right ) &= g\left(z-1 \right ) - 2g \left(z \right ) + g\left( z+1 \right ),
\end{align}
kde $P$ je počet snímků, $N$ počet pixelů v~jednom snímku a $Z_{min}$ a $Z_{max}$ nejnižší a nejvyšší hodnota digitálního snímku.
Rovnici (\ref{eq:ls}) lze parafrázovat jako nalezení $\left( Z_{max} - Z_{min} + 1\right)$ hodnot funkce $g \left( Z\right)$ a $N$ hodnot $\ln \left( E_i\right) $, které minimalizují účelovou funkci $O$.
První část rovnice je aplikace metody nejmenších čtverců na rovnice podle (\ref{eq:debevec_char_ap_fin}), druhá část rovnice ${g}''$reprezentuje vyhlazovací operátor.
Problém definovaný v~(\ref{eq:ls}) může být vyřešen dle \cite{Debevec} například pomocí metody singulárního rozkladu -- Singular Value Decomposition (SVD).

Autor této metody dodává, že je nutné uvážit dvě skutečnosti.
První ze skutečností je, že řešení rovnice \ref{eq:ls} lze nalézt pouze pro hodnoty $E_i$ a $g\left( z~\right)$ nižší než $E_i + \alpha$ a $g\left( z~\right)+ \alpha$, kde $\alpha$ je tzv. scale faktor.
V~případě, že tato podmínka není splněna, systém rovnic a účelová funkce $O$ zůstanou nezměněny.
Tento problém lze vyřešit zavedením (\ref{eq:scale_factor}) do lineárního systému.
Díky zavedení této rovnice budou mít pixely s~hodnotou přesně mezi $Z_{max}$ a $Z_{min}$ jednotkovou expozici. \cite{Debevec}
\begin{align}
\label{eq:scale_factor}
g \left( Z_{min} \right) = 0 \quad, kde \quad Z_{mid} = \frac{1}{2} \left( Z_{min} + Z_{max} \right) 
\end{align}
Druhá ze skutečností je, že obecný tvar odezvy detektoru $g \left( z~\right)$ bude strmější v~blízkosti $Z_{max}$ a $Z_{min}$ a díky tomu bude výsledná odezva méně vyhlazená v~krajních bodech.
Tento problém lze vyřešit zavedením váhové funkce $w \left( z\right)$:
\begin{align}
\label{eq:w}
\left\{\begin{matrix}
z~- Z_{min} & pro \quad z~\leq \frac{1}{2} \left(Z_{min} + Z_{max} \right )\\ 
Z_{max} - z~& pro \quad z~> \frac{1}{2} \left(Z_{min} + Z_{max} \right )
\end{matrix}\right.
\end{align}
Dosazením váhové rovnice \ref{eq:w} do (\ref{eq:ls}) dostaneme finální rovnici pro nalezení $g$ a $E_i$:
\begin{align}
\label{eq:ls_final}
O~&= \sum_{i=1}^{N} \sum_{j=1}^{P} \left \{ w \left(Z_{ij} \right ) \left [g \left(Z_{ij}-\ln E_i - \ln\Delta t_j \right ) \right ] \right \}^2 
+ \lambda \sum_{z=Z_{min}+1}^{Z_{max}-1}\left [w \left(Z_{ij} \right ){g}'' \left(z \right ) \right ]^2
\end{align}
Implementaci získání odezvy detektoru v~matlabu pomocí výše popsané metody včetně vyřešení rovnice \ref{eq:ls_final} pomocí SVD, je možné nalézt v~práci autora této metody \cite{Debevec}.

\subsubsection{Vytvoření HDR snímku}
Po získání funkce odezvy detektoru je rekonstrukce HDR snímku již poměrně snadná.
Reálné ozáření detektoru lze vypočítat jako:
\begin{align}
\label{eq:calc}
\ln E_i=\frac{\sum_{j=1}^{P} w \left(Z_{ij} \right ) \left(g \left(Z_{ij} \right ) - \ln \Delta t_j\right )}{\sum_{j=1}^{P} w \left(Z_{ij} \right )},
\end{align}
kde $w$ je váhová funkce popsaná rovnicí \ref{eq:w}, které dává vyšší váhu pixelům, které odpovídají středu funkce odezvy detektoru $g$.
