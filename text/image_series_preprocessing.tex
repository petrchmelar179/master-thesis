\chapter{Předzpracování série rentgenových snímků}
\label{sec:image-series-preprocessing}
V~následující kapitole je popsán návrh a implementace metod pro předzpracování série rentgenových snímků za účelem odstranění šumu a zvýšení dynamického rozsahu.
V~kapitole jsou implementovány a zhodnoceny metody, které jsou založeny na teoretických poznatcích z~kapitoly \ref{sec:preprocessing-methods}.
Implementace je provedena v~jazyce Python v~prostředí Jupyte notebook.

\section{Pořízení testovacích snímků}
\label{sec:test_data}
Za účelem ověření implementovaných postupů byla nasnímána testovací data.
Jako zdroj záření byla zvolena rentgenka Spellman XRB011 \cite{spellman-xrb011} s~maximálním napětím v~rozmezí od \SI{35}{\kV} do \SI{80}{\kV} a žhavícím proudem od \SI{0}{\micro\ampere} do \SI{700}{\micro\ampere}.
Snímání záření poté obstarává flat-panel s~CMOS snímači Shad-o-Box 1548 HS \cite{SB1548HS} od společnosti Teledyne Dalsa.
Rozlišení detektoru je $1032 \times 1548$ a velikost snímací plochy je $\SI{10.2}{\cm} \times \SI{15.3}{\cm}$. Maximální frekvence snímkování je 30 snímku za vteřinu.
Detektor využívá 14 bitové AD převodníky, tudíž maximální možná hodnota snímku je $2^14 - 1$.
Výstupní snímek je reprezentován jako matice s~prvky o~velikosti 16 bitů v~kterých jsou uloženy 14 bitová data, tudíž jsou horní dva bity nulové.

Jak rentgenka, tak detektor jsou zabudovány do šasi rentgenového přístroje SCIOX SMT\cite{sciox} od společnosti ELEDUS.
K~pořízení snímků byla využita aplikace, která je dodávána společně s~přístrojem. Obstarává komunikaci s~rentgenkou a detektorem.
Pořízené testovací snímky byly uloženy ve formátu tiff jako 14 bitová data, která jsou uložena v~16 bitech.
V~případě prohlížení testovacích snímků uložených v~tiff je třeba brát v~potaz, že software pro prohlížení snímků interpretuje 14 bitová data jako 16 bitová, proto se snímky mohou jevit jako tmavé.
Některý software pro prohlížení snímků dokáže snímky automaticky normalizovat do celého 16 bitového rozsahu.
Rozdíl mezi podobou normalizovaného a nenormalizovaného snímku při prohlížení v~různých softwarech ukazuje \cref{fig:windows-irfan-comparsion}.
\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{windows-irfan-comparsion}
	\caption{Porovnání zobrazení stejného 14 bitového snímku alokovaného v~16 bitech ve IrfanView (vlevo -- s~normalizací) a Windows Photo Viewer (vpravo -- bez normalizace).}
	\label{fig:windows-irfan-comparsion}
\end{figure}


\section{Průměrování po registraci (AAR)}
\label{sec:aar}
Jednou z~metod pro odstranění šumu sloučením série snímků je průměrování (kapitola \ref{sec:averaging}).
Tato metoda je citlivá na posun rentgenovaného předmětu ve scéně, kdy při zprůměrování takto pořízených snímků, vznikají ve výsledném snímku neostrosti a artefakty způsobené posunem rentgenovaného objektu ve scéně.
Zvýšení odolnosti průměrovací metody na posuny objektu ve scéně může být řešeno pomocí metody využívající obrazové registrace.

Hlavní problém, který obrazová registrace řeší, je hledání stejných klíčových bodů mezi páry snímků $\left\{ I_{i}, I_{j} \right\}$ pro $j = \left \{0, 1, ..., N - 1 \right \} - \left \{i \right \}$, kde $I$ je množina všech snímků, $i$ je index základního snímku, vůči kterému jsou hledány klíčové body ve snímcích $I_{j}$ a $N$ je celkový počet snímků v~množině $I$.
Jinými slovy obrazová registrace spočívá v~nalezení stejných klíčových bodů mezi základním snímkem například $S_{0}$ a ostatními snímky $S_{j}$ z~množiny všech snímků $I$.
K~popisu klíčových bodů využívá obrazová registrace deskriptorů.
Po nalezení stejných klíčových bodů je nutné na základě těchto bodů transformovat snímek $S_j$ do souřadnicové soustavy základního snímku.
Po transformaci všech snímků do souřadnicové soustavy základního snímku je možné všechny snímky zprůměrovat podle rovnice \eqref{eq:average} a tím odstranit šum.
Tento postup popisuje levá část diagramu v~obrázku \ref{fig:AAR-flowchart-implementation}.

\begin{figure}[h]
	\centering
	\includegraphics[width=12cm]{AAR-flowchart-implementation}
	\caption{Vývojový diagram AAR s~konkrétně zvolenými metodami pro jednotlivé operace. \zk{zk:AAR}.}
	\label{fig:AAR-flowchart-implementation}
\end{figure}

\subsection{Testovací snímky pro AAR}
Pomocí sestavy popsané v~kapitole \ref{sec:test_data} bylo nasnímáno 20 stejných snímků v~časovém rozmezí \SI{20}{\second} mezi jednotlivými snímky.
Snímky byly nasnímány s~napětím \SI{60}{\kV} a proudem \SI{250}{\micro\ampere} na rentgence při době expozice \SI{30}{\ms}.
Tři ze série zašumělých snímků lze vidět na obrázku \ref{fig:aar-series}.
V~detailu snímku je patrný již zmiňovaný šum s~Poissonovým náhodným rozdělením.
U~všech testovacích snímků je provedeno předzpracování zmenšením rozlišení.
Rozlišení je zmenšeno trojnásobně pomocí lokálního průměru.

\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{aar-series}
	\caption{Tři snímky ze série zašumělých snímků zapalovače s~posunem objektu ve scéně.}
	\label{fig:aar-series}
\end{figure}

\subsection{Ověření funkčnosti metody AAR}
\label{sec:aar-verification}
Za pomocí metody ORB pro nalezení klíčových bodů a jejich popis, která byla popsána v~kapitole \ref{sec:preprocessing-methods}, lze implementovat algoritmus pro odstranění šumu průměrováním.
Navržený algoritmus pro ověření funkčnosti navrhované metody ukazuje obrázek \ref{fig:AAR-flowchart-implementation}. 
Navržený postup je implementován pomocí jazyka Python \footnote{Interpretovaný objektově orientovaný programovací jazyk \cite{python}} v~prostředí Jupyter notebook \footnote{Prostředí umožňující vytvářet a spouštět části Python kódu \cite{jupyter}}.
V~implementaci je využito balíčků scikit-image \footnote{Python balíček pro práci s~obrázky \cite{scikit-image}},  matplotlib \footnote{Python balíček pro zobrazování grafů \cite{matplotlib}} a numpy \footnote{Python balíček pro matematické operace \cite{numpy}}.

\subsubsection{Výsledky aplikace AAR pro odstranění šumu}
Na sérii zašumělých dat byl aplikován algoritmus obyčejného průměrování a průměrování po registraci (\cref{fig:averaging-result}).
Z~výsledků je zřejmé, že algoritmus pro obrazovou registraci funguje správně a dokáže správně transformovat všechny snímky.
Na snímku vlevo se nachází v~horní a dolní části snímku artefakty v~podobě tmavých oblastí.
Tyto artefakty jsou způsobeny nedostatkem informací při posunu snímků, jejichž rentgenované objekty jsou mimo snímanou plochu (například třetí snímek v~obrázku \ref{fig:aar-series}).
Neznámé informace při posunu snímků jsou algoritmem nahrazovány krajními hodnotami snímku pro danou stranu.
Při porovnáním detailu v~obrázku s~testovacími daty \ref{fig:aar-series} a v~obrázku s~výsledky \ref{fig:averaging-result}, je patrné potlačení šumu, které je důsledkem aplikace AAR.

Implementací a aplikováním metody AAR se podařilo navrhnout robustní metodu pro odstranění náhodného šumu v~rentgenových snímcích. Tato metoda je odolná proti posunu a rotaci snímaného objektu v~pořízeném snímku.
Díky této vlastnosti lze implementovanou metodu využít například pro odstranění šumu při rentgenování pohybujícího se objektu.

\begin{figure}[htb]
\centering
\includegraphics[width=\textwidth]{averaging-result}
\caption{Výsledek průměrování s~obrazovou registrací (vlevo) a bez obrazové registrace (vpravo).}
\label{fig:averaging-result}
\end{figure}

\section{Zvýšení dynamického rozsahu Debevecovou metodou}
\label{sec:hdr-debevec}
Dynamický rozsah snímaných snímků lze zvýšit díky metodám, které slučují sérii snímků pořízených s~různými expozičními parametry.
Jedna z~takových metod byla popsána v~kapitole \ref{sec:debevec-hdr}.
Zmiňovaná metoda je rozdělena do dvou kroků.
V~prvním kroku je pomocí kalibračních dat vypočítána charakteristická křivka detektoru rentgenového záření.
V~druhém kroku je tato křivka využita ke sloučení série rentgenových snímků, které byly nasnímány s~rozdílnými expozičními parametry.

Rekonstruovaný HDR snímek je reprezentovaný maticí čísel s~pohyblivou řádovou čárkou, tudíž musí být pro jeho zobrazení či uložení pomocí standardních technologií (například uložení ve formátu png a zobrazení na běžném monitoru) upraven.
Tato úprava je prováděna pomocí tzv. tone mappingu, jehož cílem je co nejvěrnější převod snímku z~oblasti s~pohyblivou řádovou čárkou do oblasti standardní (zpravidla snímek s~8 nebo 16 bitovou hloubkou).

\subsection{Testovací snímky pro Debevecovu metodu}
Pro ověření možnosti aplikace Debevecovy metody na sérii rentgenových snímků pro rekonstrukci HDR snímku jsou nasnímány dvě série snímků: kalibrační snímky a snímky pro rekonstrukci HDR snímku.
Obě série snímků by měly obsahovat snímky s~postupně rostoucími dobami expozice $\Delta t$.
Dle \cite{Debevec} by měla být doba expozice snímku v~sérii vždy dvojnásobná oproti době expozice snímku předchozího.
Pro snímání testovacích snímků je využita sestava, která byla popsána v~kapitole \ref{sec:test_data}.
Stejně jako v~kapitole \ref{sec:averaging} jsou jako napětí a proud rentgenky zvoleny hodnoty \SI{60}{\kV} a \SI{250}{\micro\ampere}.

\subsubsection{Kalibrační snímky}
Pro správnou rekonstrukci musí být kalibrační snímky nasnímány tak, aby každý snímek obsahoval alespoň jednu přeexponovanou a jednu podexponovanou oblast.
Zároveň by snímky měly obsahovat oblast, jejíž intenzita se mění se změnou doby expozice.
S~parametry popsanými výše je nasnímá série snímků s~dobami expozice \SI{20}{\ms}, \SI{40}{\ms}, \SI{80}{\ms}, \SI{160}{\ms}, \SI{320}{\ms}, \SI{640}{\ms} a \SI{1280}{\ms}.

Podexponovaná oblast je zajištěna zastíněním části detekční plochy detektoru olověným plátem, přeexponované oblasti jsou oblasti přímého dopadu rentgenového záření na detektor.
Oblasti s~měnící se intenzitou při změně expoziční doby $\Delta t$ jsou zajištěny dvěma překrývajícími měděnými plíšky.
Sérii kalibračních snímků lze vidět na obrázku \ref{fig:hdr-calib-series}.

\begin{figure}[h]
	\centering
	\includegraphics[width=\linewidth]{obrazky/hdr-calib-series}
	\caption{Série kalibračních snímků s~dobami expozice \SI{20}{\ms}, \SI{40}{\ms}, \SI{80}{\ms}, \SI{160}{\ms}, \SI{320}{\ms}, \SI{640}{\ms} a \SI{1280}{\ms}.}
	\label{fig:hdr-calib-series}
\end{figure}

\subsubsection{Testovací snímky}
Stejně jako v~případě testovacích snímků pro AAR, je zvolen jako rentgenovaný objekt zapalovač.
Zapalovač je díky materiálům, z~kterých je vyroben, vhodný pro ověření správného fungování zvolené metody.
Očekávaným výsledkem po aplikaci Debevecovy metody je výstupní snímek, na kterém bude zřejmá struktura plastového zásobníku a zároveň struktura vrchní části zapalovače s~překrývajícími se kovovými částmi.

Testovací snímky jsou pořízeny tak, aby snímek s~nejnižší dobou expozice obsahoval dobře viditelné plastové části.
V~tomto případě jsou kovové části testovaného objektu podexponované.
Naopak v~případě snímku s~nejvyšší dobou expozice, by měly být dobře viditelné struktury kovových částí a přeexponované části z~plastu.
Sérii testovacích snímků ukazuje obrázek \ref{fig:hdr-test-series}.

\begin{figure}[h]
	\centering
	\includegraphics[width=\linewidth]{obrazky/hdr-test-series}
	\caption{Série testovacích snímků s~dobami expozice \SI{40}{\ms}, \SI{80}{\ms}, \SI{160}{\ms}, \SI{320}{\ms} a \SI{640}{\ms}.}
	\label{fig:hdr-test-series}
\end{figure}

\subsection{Ověření funkčnosti Debevecovy metody}
\label{sec:hdr-debevec-verification}
Na základě Debevecovy metody lze implementovat algoritmus pro získání snímku s~vysokým dynamickým rozsahem ze série snímků pořízených s~různou expoziční dobou.
Diagram popisující postup při aplikaci této metody popisuje obrázek \ref{fig:hdr-implementation}.

Postup z~diagramu je implementován pomocí jazyka Python, v~prostředí Jupyter notebook.
V~implementaci je využito balíčku scikit-image pro práci se snímky a balíčku OpenCV pro python, ve které je Debevecova metoda již implementována.
Snímky jsou zobrazeny pomocí balíčku Matplotlib a pro práci s~maticemi je využit balíček numpy.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.75\linewidth]{HDR-implementation.pdf}
	\caption{Vývojový diagram implementace metody HDR.}
	\label{fig:hdr-implementation}
\end{figure}

\subsection{Předzpracování snímků}
Implementace Debevecovy metody v~knihovně OpenCV, potažmo python balíčku pro práci s~OpenCV umí pracovat pouze s~daty 
v~tří-kanálovém BGR (Blue, Green a Red) modelu s~8-bitovou hloubkou, proto je nutné sérii testovacích a kalibračních snímků převést z~jednokanálového 14-bitového snímku alokováného v~16 bitech do podoby, se kterou umí OpenCV pracovat.
Převod byl implementován tak, že 14 bitové snímky byly převedeny na snímky s~8-bitovou hloubkou.
Takto upravené snímky byly použity k~vytvoření všech tří kanálů BGR snímku -- všechny kanály byly nahrazeny upravenými 8-bitovými snímky.

Předzpracování série snímků ovlivní výsledek pouze ve smyslu ztráty informace při převodu snímků z~14-bitové datové hloubky do 8-bitové.
Vzhledem k~tomu, že OpenCV implementace funkcí pro získání charakteristiky detektoru i sloučení snímku v~HDR snímek pracují pro každý kanál zvlášť, lze výstup těchto funkcí považovat za relevantní.
Ve všech kanálech výsledného snímku by se měla nacházet stejná výstupní data.
Z~těchto důvodů, lze ověření funkčnosti Debevecovy metody pro rentgenové snímky pomocí funkcí implementovaných v~OpenCV považovat za dostatečné.

\subsection{Získání charakteristické křivky a sloučení snímků}
\begin{figure}[h!]
	\centering
	\includegraphics[width=\linewidth]{obrazky/hdr-response-opencv}
	\caption{Charakteristická křivka detektoru získaná pomocí OpenCV.}
	\label{fig:hdr-response-opencv}
\end{figure}
Kalibrační křivka může být získána pomocí OpenCV vytvořením kalibrační instance.
\begin{samepage}
Instance má několik parametrů, které lze nastavit při její konstrukci:
\begin{itemize}
	\item Počet vybraných pixelů pro kalibraci.
	\item Parametr $\lambda$ vyhlazovacího operátoru.
	\item Způsob výběru pixelů pro kalibraci.
\end{itemize}
\end{samepage}
Pro kalibraci byla vytvořena instance s~parametry: počet vybraných pixelů 200 a vyhlazovací parametr 10.
Jako způsob výběru pixelů pro kalibraci byl vybrán rovnoměrný výběr pomocí mřížky.
Výslednou odezvu detektoru vypočítanou ze série kalibračních snímků pomocí metody implementované v~OpenCV ukazuje obrázek \ref{fig:hdr-response-opencv}.
V~obrázku je zobrazen pouze jeden kanál, jelikož ostatní jsou z~důvodů popsaných výše totožné.
Díky získané charakteristické křivce detektoru je možné sloučit testovací snímky pomocí implementace v~OpenCV.
Pro možnosti zobrazení a uložení HDR snímku byl na výstupní snímek aplikován pomocí OpenCV Dragův tone mapping \cite{Drago}.
Výsledný HDR snímek po aplikaci tone mappingu ukazuje obrázek \ref{fig:hdr-result-opencv}

\subsection{Výsledky aplikace Debevecovy metody}
\begin{figure}[h]
	\centering
	\includegraphics[width=0.5\linewidth]{obrazky/hdr-result-opencv}
	\caption{Výsledný snímek po aplikaci Debevecovy metody a Dragova tone mappingu získaný pomocí OpenCV.}
	\label{fig:hdr-result-opencv}
\end{figure}
Na předzpracovaná data byl aplikován Debevecův algoritmus pomocí implementace v~OpenCV.
Nejdříve byla získána funkce odezvy detektoru $g$, která byla využita pro sloučení série testovacích dat v~HDR snímek.
Na HDR snímek byl následovně aplikován Dragův tone mapping.
Výsledný snímek lze vidět na obrázku \ref{fig:hdr-result-opencv}

Na snímku lze pozorovat zvýšený dynamický rozsah, který se projevuje dobrým kontrastem v~původně podexponovaných či přeexponovaných oblastech v~sérii testovacích snímků.
Zvýšený kontrast v~těchto oblastech umožňuje sledovat části rentgenovaného objektu, které jsou vyrobeny z~materiálu s~vysokou absorpcí rentgenového záření (kov) a nízkou absorpcí rentgenového záření (plast) zároveň. 
Snímek ukazuje krom zvýšeného dynamického rozsahu, také snížený šum díky skládání více snímků v~jeden -- podobný princip jako v~případě průměrování, kterému byla věnována předešlá kapitola \ref{sec:aar}.