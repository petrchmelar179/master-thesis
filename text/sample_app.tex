\chapter{Ukázková aplikace}
\label{sec:sample-app-implementation}
Tato kapitola se zabývá návrhem a implementací ukázkové aplikace.
Mimo návrhu a implementace ukázkové aplikace je v~rámci kapitoly navržena a implementována služba pro předzpracování série rentgenových snímků.
V~první části kapitoly je proveden návrh jak služby, tak aplikace.

Druhá část kapitoly se zabývá již samotnou implementací.
V~rámci této části je popsán způsob, jakým byly služba a aplikace implementovány a způsoby distribuce obou aplikací.
Na konci kapitoly jsou zhodnoceny výsledky předzpracování snímků pomocí implementovaných částí a porovnány s~výsledky z~kapitoly \ref{sec:image-series-preprocessing-implementation}.

\section{Návrh}
Cílem návrhu ukázkové aplikace je navrhnout, jakým způsobem budou implementovány jednotlivé části výsledné aplikace.
Návrh se zabývá třemi hlavními částmi: ukázkovou aplikací, službou před-zpracovávající sérii snímků a komunikací mezi ukázkovou aplikací a službou.
Návrh by se mimo jiné zaměřuje na dobré oddělení ukázkové aplikace od předzpracování série snímků, z~důvodu snadného nahrazení služby předzpracování jinou implementací.

Diagram návrhu celého systému lze vidět na obrázku \ref{fig:sample-app-diagram-design-design}.
Uživatel dává příkazy ukázkové aplikaci, která umí zpracovat vstupní DICOM soubory pomocí implementované knihovny.
Příkazy jsou vyhodnoceny ukázkovou aplikací, na základě čehož jsou posílány dotazy službě předzpracování rentgenových snímků.
Snímky jsou předzpracovány pomocí volání implementovaných metod pro AAR a HDR.
Výstup z~volaných metod je zaslán jako odpověď ukázkové aplikaci, která odpověď uloží jako nový DICOM soubor do souborového systému na základě vstupních parametrů od uživatele.

\begin{figure}[h]
	\centering
	\includegraphics[width=\linewidth]{obrazky/sample-app-diagram-design}
	\caption{Návrh ukázkové aplikace se službou pro předzpracování série rentgenových snímků.}
	\label{fig:sample-app-diagram-design-design}
\end{figure}

\subsection{Komunikace ukázkové aplikace s~předzpracováním série snímků}
Pro možnost komunikace ukázkové aplikace s~implementací předzpracování série rentgenových snímků, která byla implementována v~kapitole \ref{sec:image-series-preprocessing} je vhodné navrhnout komunikační rozhraní.
Navržené komunikační rozhraní umožní snadnou změnu či náhradu části pro předzpracování série rentgenových snímků.

\subsubsection{gRPC}
Pro komunikaci mezi ukázkovou aplikací a metodami pro předzpracování snímků je využito technologie gRPC.
Tato technologie umožňuje snadný návrh a implementaci meziprocesové komunikace.
Pro přenos dat je využíváno TCP/IP spojení.
Přenášená data jsou serializovaná pomocí technologie Protocol Buffers od google.

Technologie definuje službu jako soubor metod, které mohou být vzdáleně volány.
Na základě těchto definic je vygenerován kód pro jeden z~podporovaných programovacích jazyků.
Technologie také definuje dvě strany komunikace: server a klient.
Server implementuje volání metod, které jsou definovány službou, přičemž klient může implementované metody vzdáleně volat.
Jak klient, tak server jsou implementovány pomocí vygenerovaného kódu, který je vygenerován z~definice služby.

Služba je definována pomocí programovacího jazyka navrženého v~rámci Protocol Buffers.
takto definovaná služba může být distribuována do různých částí projektu jako vygenerovaný kód.
Kód může být vygenerován pro několik programovacích jazyků včetně C\# a Python, pomocí kterých jsou implementována knihovna pro práci s~DICOM soubory a předzpracování série snímků.

\subsubsection{Návrh komunikačního rozhraní}
Pomocí gRPC, které bylo popsáno výše, je navržen protokol pro komunikaci ukázkové aplikace s~metodami pro předzpracování rentgenových snímků.
Předzpracování série snímků bude implementováno jako služba poskytující všechny dostupné metody.
Ukázková aplikace se bude dotazovat pomocí kódu, který bude vygenerován na základě definice služby.
Maximální možná velikost zprávy, která může být zaslána v~rámci jednoho dotazu jsou \SI{4}{\byte}.
Tato hodnota může být v~případě nutnosti navýšena.

\paragraph{Zpráva reprezentující snímek}
Pro přenos snímků je nejprve třeba definovat jeho zprávu. (výpis \ref{list:image-message-definition})
V~rámci zprávy je definován počet řádků a sloupců snímku jako \textit{Rows} a \textit{Columns}.
Bitová hloubka snímku je reprezentována hodnotou proměnné \textit{BitsAllocated}.
Pro možnost reprezentace snímků, které jsou reprezentovány 14-ti bity, ale jsou uloženy v~16-ti bytech, je navržena proměnná \textit{BitsStored}.
Doba expozice v~milisekundách je vyjádřena hodnotou proměnné \textit{ExposureMs}.
Data snímku jsou uloženy jako byty v~proměnné \textit{PixelData}.
Všechny proměnné zprávy kromě \textit{PixelData}, která má datový typ \textit{bytes}, mají datový typ \textit{uint32}.
\begin{listing}[h]
\begin{minted}{proto}
message PixelData{
	uint32 Rows = 1;
	uint32 Columns = 2;
	uint32 BitsAllocated = 3;
	uint32 BitsStored = 4;
	uint32 ExposureMs = 5;
	bytes PixelData = 6;
}
\end{minted}
\caption{Definice zprávy snímku.}
\label{list:image-message-definition}
\end{listing}

\paragraph{Zprávy reprezentující dotazy}
Navrhovaná služba definuje tři dotazy, které reprezentují volání jednotlivých metod předzpracování série snímků.
Definici dotazů ukazuje výpis \ref{list:request-message-definition}.
Zpráva \textit{AarRequest} je dotaz na metodu pro AAR, \textit{HdrRequest} je dotaz na metodu pro HDR a \textit{HdrCalibrationRequest} je dotaz na metodu pro kalibraci.
Všechny definované dotazy obsahují pole zpráv typu \textit{PixelData}, které reprezentuje sérii snímku k~předzpracování.
\begin{listing}[h]
\begin{minted}{proto}
message AarRequest { repeated PixelData AarPixelData = 1; }
message HdrRequest { repeated PixelData LdrPixelData = 1; }
message HdrCalibrationRequest { repeated PixelData LdrPixelData = 1; }
\end{minted}
\captionof{listing}{Definice zprávy dotazu.}
\label{list:request-message-definition}
\end{listing}
\paragraph{Zprávy reprezentující odpovědi}
Definované dotazy musí mít definovány odpovědi.
Definici odpovědí na dotazy ukazuje výpis \ref{list:response-message-definition}.
Zpráva \textit{HdrResponse} je odpověď na metodu HDR, \textit{AarResponse} je odpověď na metodu AAR a \textit{HdrCalibrationResponse} je odpověď na kalibrační metodu pro HDR.
Odpověď na HDR a AAR obsahují jednu zprávu definovanou jako \textit{PixelData}, která reprezentuje výsledný snímek po aplikaci metod.
Odpověď na metodu pro kalibraci HDR obsahuje pole hodnot s~plovoucí desetinnou čárkou, které reprezentují vypočítanou charakteristickou křivku detektoru.
\begin{listing}[h]
\begin{minted}{proto}
message HdrResponse {
	PixelData HdrPixelData = 1;
}
message AarResponse {
	PixelData AarPixelData = 1;
}
message HdrCalibrationResponse {
	repeated float g = 2;
}
\end{minted}
\captionof{listing}{Definice zprávy odpovědi.}
\label{list:response-message-definition}
\end{listing}
\paragraph{Definice služby}
Služba je definována pomocí dotazů a odpovědí, které jsou definovány. (výpis \ref{list:service-definition})
Zpráva \textit{HdrCalibration}, \textit{HdrRequest} a \textit{AarRequest} jsou rozhraní služby, které musí být implementovány na straně serveru a můžou být volány vzdáleně pomocí klienta.
\begin{listing}[h]
\begin{minted}{proto}
service Improc {
	rpc HdrCalibration (HdrCalibrationRequest) 
		returns (HdrCalibrationResponse) {}
	rpc Hdr (HdrRequest) returns (HdrResponse) {}
	rpc Aar (AarRequest) returns (AarResponse) {}
}
\end{minted}
\captionof{listing}{Definice služby.}
\label{list:service-definition}
\end{listing}
\subsection{Návrh ukázkové aplikace}
Ukázková aplikace by měla umět načítat DICOM soubory pomocí knihovny, která byla navržena v~kapitole \ref{sec:dicom-library-implementation}.
Implementace knihovny cílí na .NET standard 2.0, díky tomu lze pro implementaci ukázkové aplikace vybírat z~několika dostupných implementací .NET:
\begin{itemize}
	\item .NET Core 2.0,
	\item .NET Framewrok (s~.NET Core 2.0 SDK) 4.6.1,
	\item Mono 5.4,
	\item Xamarin.iOS 10.14,
	\item Xamarin.Mac 3.8,
	\item Xamarin.Android 8.0 nebo
	\item Universal Windows Platform 10.0.16299.
\end{itemize}
Z~těchto možností je pro implementaci ukázkové aplikace vybrána implementace .NET Core 2.0.
Tato implementace byla vybrána především díky možnosti kompilovat program napsaný v~C\# jako spustitelný soubor pro Windows, linuxové operační systémy a macOS.

Ukázková aplikace bude sloužit jako klient k~serveru, který bude implementovat službu sloužící k~předzpracování série rentgenových snímků.
Aplikace by měla umět načítat vstupní data od uživatele, reagovat na příkazy a vykonávat je pomocí vzdáleného volání, které bude poskytováno službou.

\subsubsection{Návrh uživatelského rozhraní ukázkové aplikace}
Pro komunikaci ukázkové aplikace s~uživatelem je navrženo jednoduché textové rozhraní, které je přístupné z~příkazového řádku operačního systému.
Ukázková aplikace čte parametry předané při spuštění aplikace na příkazovém řádku a na základě jejich zpracování provádí příslušné akce.

V~rámci aplikace budou implementovány 3 příkazy, které lze volat: hdrcalib, hdr, a aar.
Všechny tři příkazy slouží jako komunikační rozhraní se službou zpracovávající sérii rentgenových snímků.

\paragraph{Příkaz hdrcalib}
První z~příkazů je příkaz sloužící ke kalibraci služby pro HDR.
Příkaz by měl být zavolán před voláním příkazů pro rekonstrukci HDR snímku.
Tento příkaz bere pouze jeden parametr a to adresář se vstupními soubory.
Parametr je značen pomocí \textit{\texttt{-{}-}input} nebo \textit{-i}.
V~případě, že adresář neexistuje nebo neobsahuje alespoň dva DICOM soubory, ukázková aplikace skončí s~chybou.
Správné volání aplikace s~příkazem hdrcalib ukazuje výpis \ref{list:hdrcalib-calling}.
\begin{listing}[h]
\begin{minted}{bat}
Eledus.Sciox.Dicom.SampleApp.exe hdrcalib --input calibration_images\
\end{minted}
\caption{Volání příkazu hdrcalib.}
\label{list:hdrcalib-calling}
\end{listing}
\paragraph{Příkaz hdr}
Pro rekonstrukci HDR snímků slouží příkaz hdr.
Tento příkaz by neměl být volán před provedení kalibrace pomocí příkazu hdrcalib.
Příkaz bere dva parametry: adresář se snímky s~nízkým dynamickým rozsahem označen pomocí \textit{\texttt{-{}-}input} nebo \textit{-i} a cestu pro uložení výsledného HDR snímku s~označeného jako \textit{\texttt{-{}-}output} nebo \textit{-o}.
Stejně jako v~případě příkazu hdrcalib je aplikace ukončena s~chybovou hláškou v~případě, že adresář se vstupními soubory neexistuje nebo neobsahuje alespoň dva DICOM soubory.
Příklad příkazu pro rekonstrukci HDR snímku ukazuje výpis \ref{list:hdr-calling}.
\begin{listing}[h]
\begin{minted}{bat}
Eledus.Sciox.Dicom.SampleApp.exe hdr --input ldr\ --output res.dcm
\end{minted}
\caption{Volání příkazu hdr.}
\label{list:hdr-calling}
\end{listing}
\paragraph{Příkaz aar}
Poslední z~příkazů slouží pro aplikaci AAR na sérii snímků, předaných pomocí parametru značeného jako \textit{\texttt{-{}-}input} nebo \textit{-i}.
Cesta k~uložení výsledku je předána jako parametr, který je označen jako \textit{\texttt{-{}-}output} \textit{-o}.
Stejně jako v~předchozích případech, v~případě, že jako vstupní parametr je předán adresář, který neexistuje nebo neobsahuje alespoň dva DICOM snímky, je aplikace ukončena s~chybou.
Správné volání příkazu aar ukazuje výpis \ref{list:aar-calling}.
\begin{listing}[h]
\begin{minted}{bat}
Eledus.Sciox.Dicom.SampleApp.exe aar --input images\ --output res.dcm
\end{minted}
\caption{Volání příkazu aar.}
\label{list:aar-calling}
\end{listing}
\section{Implementace}
Tato část kapitoly popisuje implementaci, která je provedena na základě návrhu v~předešlé kapitole.
Implementace se zabývá vytvořením serveru, který implementuje navrženou službu vytvořenou pomocí technologie gRPC a klienta, který se služby dotazuje.
Klient je implementován ukázkovou aplikací s~uživatelským rozhraním, která využívá knihovnu navrženou a implementovanou v~kapitole \ref{sec:dicom-library-implementation}.
Diagram implementovaného systému ukazuje obrázek \ref{fig:sample}.
\begin{figure}[h]
	\centering
	\includegraphics[width=\linewidth]{obrazky/sample-app-diagram}
	\caption{Implementace ukázkové aplikace a sužby pro předzpracování série rentgenových snímků.}
	\label{fig:sample}
\end{figure}
\subsection{Služba pro předzpracování série snímků}
\label{sec:improc-service}
Server, který implementuje definovanou službu, je vytvořen v~programovacím jazyce Python.
Implementace využívá funkcí pro předzpracování série rentgenových snímků, které byly implementovány v~kapitole \ref{sec:image-series-preprocessing-implementation}.
\subsubsection{Implementace služby}
Implementace služby serverem je založena na kódu, který byl vygenerován na základě její definice.
Tento kód obsahuje třídu \textit{ImprocServicer}, která musí být znovu implementována pomocí přetížení metod z~výpisu \ref{list:improcservicer-methods}.
Zmíněné metody jsou automaticky volány ve chvíli, kdy klient zašle konkrétní dotaz serveru.
V~rámci metod musí být vykonány příslušné operace a zaslána odpověď, která je předána pomocí návratové hodnoty metod.
Metody berou jako parametr instanci dotazu \textit{request} a kontextu \textit{context}, který však v~rámci implementace není využíván.
\begin{listing}[h]
\begin{minted}{python}
def HdrCalibration(self, request, context)
def Hdr(self, request, context)
def Aar(self, request, context)
\end{minted}
\caption{Metody třídy ImprocServicer.}
\label{list:improcservicer-methods}
\end{listing}

Třída implementuje dvě pomocné statické metody, které jsou využity ke kontrole instance dotazu. (výpis \ref{list:request-check-methods})
Metody kontrolují, zda dotaz obsahuje dostatečný počet snímku a zda jsou data napříč všemi snímky konzistentní.
Pokud je nalezena chyba v~datech, metoda vyvolává výjimku, která je následně automaticky propagována až ke klientovi, který zasílal dotaz serveru (v~případě, že není výjimka zachycena v~částech volající metodu).
\begin{listing}[h]
\begin{minted}{python}
def _hdr_request_check(request)
def _aar_request_check(request)
\end{minted}
\caption{Metody pro kontrolu dotazu.}
\label{list:request-check-methods}
\end{listing}

Pro snadnou testovatelnost třídy, která implementuje definovanou službu, jsou části kódu, které slouží jako volání metod předzpracování obrazu (výpis \ref{list:preprocesing-functions}), předány jako funkce přímo v~konstruktoru třídy.
Díky tomu, je v~rámci testování možné tyto části kódu nahradit vlastní jednoduchou implementací, která vrací předem definované hodnoty.
Díky tomu je testován pouze kód implementované třídy nezávisle na metodách předzpracování snímků.
Funkce \textit{pixel\_data\_to\_numpy\_array} a  \textit{numpy\_array\_to\_pixel\_data} slouží k~převodu instance přijatých snímků \textit{PixelData} na instanci třídy numpy array a zpět.
Ostatní funkce slouží k~volání jednotlivých částí předzpracování série rentgenových snímků.
\begin{listing}[h]
\begin{minted}{python}
def pixel_data_to_numpy_array(pixel_data)
def numpy_array_to_pixel_data(numpy_array, bits_stored, exposure_ms=0)
def calibrate_hdr(calibration_images, exposures)
def apply_hdr(ldr_images, exposures, g)
def apply_aar(noised_images)
\end{minted}
\caption{Funkce pro volání předzpracování snímků.}
\label{list:preprocesing-functions}
\end{listing}

Jak již bylo zmíněno v~úvodu návrhu komunikačního rozhraní, maximální velikost jedné zprávy je \SI{4}{\mega\byte}.
Přednastavená velikost zprávy je pro přenášení několika 16-ti bitových rentgenových snímků nevhodná, proto v~rámci implementace měla být velikost zprávy zvýšena.
Nicméně během implementace bylo zjištěno, že se v~implementaci gRPC pro python nachází chyba, která znemožňuje zvýšení maximální velikosti zprávy.
Tato chyba by měla být opravena v~některé z~nově vydaných verzí knihovny pro gRPC.
Díky tomuto faktu implementovaný systém podporuje pouze zprávy s~maximální velikostí \SI{4}{\mega\byte}.

\subsection{Ukázková aplikace}
\label{sec:sample-app}
Pro implementaci ukázkové aplikace byl zvolen jazyk C\# a .NET Core 2.0.
Implementovaná aplikace umožňuje uživateli zadávat příkazy, které jsou interpretovány jako vzdálené volání metod pomocí implementované služby.

Pro zpracování argumentů z~příkazového řádku je využita knihovna CommandLine, která umí automaticky deserializovat vstupní argumenty, tak aby odpovídaly navržené třídě vstupních argumentů.
Návrhem třídy vstupních argumentů lze definovat jejich datový typ, to jestli jsou povinné nebo jejich název.
Knihovna mimo jiné automaticky vytváří obrazovku s~nápovědou, která je dostupná pomocí parametru \textit{\texttt{-{}-help}}.
Zaznamenávání textového výstupu během vykonávání aplikace je zajištěno loggerem Serilog.
Logger zaznamenává textový výstup jak do uživatelského rozhraní uživatele v~podobě konzole, tak do souboru s~názvem \textit{sampleapp.log}.

Pro vzdálené volání metod služby pro zpracování snímků je použit kód, který byl vygenerován z~definice služby.
Pro volání metody služby je nejprve nutné vytvořit komunikační kanál a klienta tak, jak ukazuje výpis \ref{list:improc-client-creation}.
\begin{listing}[h]
\begin{minted}{csharp}
var channel = new Channel($"{ipAddress}:{port}",
                          ChannelCredentials.Insecure);
var client = new Improc.ImprocClient(channel);
\end{minted}
\caption{Vytvoření instance třídy ImprocClient.}
\label{list:improc-client-creation}
\end{listing}
Po vytvoření instance klienta je možné volat metody, které poskytuje služba. (výpis \ref{list:improc-client-methods-calling})
Instance pixelDataEnumerable je instancí třídy, která implementuje rozhraní \textit{IEnumerable} a reprezentuje snímky v~podobě zprávy \textit{PixelData}.
Po zavolání asynchronní metody klienta \textit{HdrAsync} s~parametrem instance dotazu \textit{request},
metoda vrátí odpověď zaslanou službou v~podobě instance třídy \textit{HdrResponse}.
V~instanci odpovědi jsou uloženy zprávy reprezentující data odpovědi.
V~případě, že dotazování služby neproběhne v~pořádku klient vyvolá výjimku třídy \textit{RpcException}, která je zachycena a aplikace validně ukončena s~chybovým kódem.
Stejným způsobem můžou být vzdáleně volány všechny metody definované službou.
\begin{listing}[h]
\begin{minted}{csharp}
var request = new HdrRequest()
{
	LdrPixelData = { pixelDataEnumerable }
};
var response = await client.HdrAsync(request);
\end{minted}
\caption{Volání metod instance klienta.}
\label{list:improc-client-methods-calling}
\end{listing}

\subsubsection{Převod mezi instancemi IDicomModel a PixelData}
Při vytváření dotazů, či čtení odpovědí je nutné mezi sebou převádět instanci třídy, která implementuje \textit{IDicomModel<IDicomDatasetWrapper>} na instanci třídy \textit{PixelData} a zpět.
Instance třídy, která implementuje \textit{IDicomModel<IDicomDatasetWrapper>} je reprezentace snímků v~rámci navržené knihovny a \textit{PixelData} je reprezentace snímků v~rámci definované služby.
Z~toho důvodu byly implementovány dvě rozšiřující metody (takzvané extension method), které lze vidět ve výpisu \ref{list:convert-methods}.
Extension method je statická metoda, která rozšiřuje funkcionalitu třídy, pro kterou je implementována.
\begin{listing}[h]
\begin{minted}{csharp}
public static PixelData ImagePixelModuleToPixelData(
     this IDicomModel<IDicomDatasetWrapper> model)
public static IDicomModel<IDicomDatasetWrapper> PixelDataToImagePixelModule(
     this PixelData pixelData)
\end{minted}
\caption{Metody pro převod mezi instancemi IDicomModel a PixelData.}
\label{list:convert-methods}
\end{listing}

\subsubsection{Volání metod definovaných službou}
Pro možnost volání metod, které jsou definovány implementovanou službou, jsou implementovány tři statické metody, které ukazuje výpis \ref{list:sample-app-methods}
Všechny metody vrací návratovou hodnotu obsahující instanci třídy \textit{Task<int>}, díky čemuž je možné metody volat asynchronně.
Po dokončení asynchronního volání, volání vrací návratovou hodnotu s~datovým typem int s~hodnotou 0 v~případě, že bylo volání dokončeno bez chyby nebo 1 v~případě, že bylo volání ukončeno s~chybou.
Jako parametr metody berou instance tříd, které reprezentují deserializované parametry příkazů z~příkazové řádky, které jsou zadány uživatelem.
\begin{listing}[h]
\begin{minted}{csharp}
public static async Task<int> Hdr(HdrOptions options)
public static async Task<int> HdrCalib(HdrCalibOptions options)
public static async Task<int> ApplyAar(AarOptions options)
\end{minted}
\caption{Implementované metody pro volání metod služby.}
\label{list:sample-app-methods}
\end{listing}

Na začátku každé metody je vytvořen klient způsobem, který byl popsán výše.
Po vytvoření klienta je ověřeno, zda adresář se vstupními soubory obsahuje dostatečný počet souborů (počet větší než jedna) a v~případě že ano, jsou soubory načteny.
Pokud jeden z~načítaných souborů neexistuje, je aplikace ukončena s~chybou.
Po načtení souborů pomocí DICOM knihovny je vytvořen příslušný dotaz a zavolána jedna z~definovaných metod služby.
Pokud je dotaz vykonán validně, je přijata odpověď se zpracovanými daty, v~opačném případě je aplikace opět ukončena s~chybou.
Přijatá data jsou v~případě \textit{Hdr} a \textit{ApplyAar} převedena zpět na DICOM soubor a uložena do adresáře na základě parametrů od uživatele.
Následně všechny metody vrací hodnotu 0 a aplikace je ukončena bez chyby.

\subsection{Distribuce ukázkové aplikace a služby předzpracování snímků}
Pro snadné spuštění celého implementovaného systému je navržen způsob distribuce.
Ukázková aplikace je distribuována jako zkompilované spustitelné soubory pro operační systémy Windows, Linux a macOS.

V~případě implementované služby není snadné implementaci distribuovat jak spustitelný soubor, proto byl vytvořen obraz pro kontejnerový systém Docker \footnote{Technologie umožňující běh aplikace v~izolovaném prostředí kontejneru \cite{docker}}, který lze stáhnout a spustit z~veřejného repositáře Docker obrazů.
Ke spuštění Docker obrazu v~Docker kontejneru je třeba mít nainstalovaný Docker.
Po nainstalování Dockeru je možné kontejner spustit tak, jak ukazuje výpis \ref{list:service-launch}.
Příkaz automaticky stáhne obraz z~veřejného repositáře a spustí server implementující službu jako lokálního hosta v~izolovaném prostředí kontejneru.
\begin{listing}[h]
\begin{minted}{bat}
docker run -it -p 50052:50052 pchmelar/improc-service
Starting improc service server...
Improc service server started.
2018-04-27 12:29:26.346808 Periodical beat
2018-04-27 12:29:31.352018 Periodical beat
\end{minted}
\caption{Spuštění Docker obrazu služby v~kontejneru.}
\label{list:service-launch}
\end{listing}

Po spuštění serveru implementující službu pro zpracování série snímků je možné využít ukázkovou aplikaci k~načtení DICOM souborů a aplikování AAR. (výpis \ref{list:sample-app-launch})
Aplikace načte DICOM soubory z~adresáře input/, zašle dotaz na metodu AAR serveru, server aplikuje metodu AAR a zašle odpověď se zpracovanými snímky, které jsou poté pomocí ukázkové aplikace uloženy jako result.dcm.
\begin{listing}[h]
\begin{minted}{bat}
Eledus.Sciox.Dicom.SampleApp.exe aar --input input\ --output result.dcm
[14:38:39 INF] GRPC client initalized.
[14:38:39 INF] aar parameters succesfully parsed.
[14:38:39 INF] Loading noised_images\[179]60kV-250uA-80ms.dcm
[14:38:39 INF] File noised_images\[179]60kV-250uA-80ms.dcm
was succesfully loaded
...
...
[14:38:39 INF] Loading noised_images\[180]60kV-250uA-80ms.dcm
[14:38:39 INF] File noised_images\[179]60kV-250uA-80ms.dcm
was succesfully loaded
[14:38:40 INF] Calling AAR request on improc service.
[14:38:46 INF] AAR request completed with succesfull response.
[14:38:46 INF] Creating new DICOM data from AAR response.
[14:38:46 INF] New DICOM data was succesfully created from AAR response.
[14:38:46 INF] Creating new file: result.dcm.
[14:38:46 INF] New file result.dcm was succesfully created.
[14:38:46 INF] Writing new DICOM data to result.dcm.
[14:38:46 INF] New DICOM data was succesfully written to result.dcm.
\end{minted}
\caption{Aplikování AAR na sérii snímků pomocí ukázkové aplikace.}
\label{list:sample-app-launch}
\end{listing}

\subsection{Výsledky ukázkové aplikace}
\begin{figure}[h]
	\centering
	\includegraphics[width=\linewidth]{obrazky/implementation-results}
	\caption{Výsledky aplikace Debevecovy metody (vlevo) a metody AAR (vpravo) získaných pomocí ukázkové aplikace.}
	\label{fig:implementation-results}
\end{figure}
Ukázková aplikace a implementovaná služba byly použity pro předzpracování stejné série snímků, jako v~kapitole \ref{sec:image-series-preprocessing-implementation}.
Stejná série byla použita z~důvodu porovnání výsledků mezi přímým voláním implementovaných metod a voláním metod pomocí ukázkové aplikace a implementované služby.
Na snímky byly aplikovány metody pro AAR, zisk charakteristické křivky detektoru a sloučení snímků v~HDR snímek.
Výsledky byly uloženy jako DICOM soubory a za účelem zobrazení ve formě obrázku v~této práci, byly vyexportovány jako 16-bitové PNG soubory.

Výsledky aplikace metod ukazuje obrázek \ref{fig:implementation-results}.
Aplikace Debevecovy vykazuje stejné výsledky jako aplikace pomocí přímého volání (obrázek \ref{fig:hdr-implementation-result}).
Sloučením série snímků byl zvýšen dynamický rozsah a zároveň částečně potlačen šum s~náhodným rozložením.
Stejně jako aplikace Debevecovy metody aplikace metody AAR vykazuje stejné výsledky jako v~případě přímého volání (obrázek \ref{fig:aar-implementation-result}).
Sloučením průměrovaných snímků byl potlačen šum s~náhodným rozložením.
Algoritmus si stejně jako v~ostatních případech dokázal poradit s~posunem rentgenovaného předmětu ve scéně.
Ve výsledném snímku se nachází stejné artefakty jako ve výsledcích přímého volání metody AAR.
Tyto artefakty jsou způsobeny nedostatkem dat při zarovnání snímku, jehož rentgenovaný předmět se nachází částečně mimo scénu.

Metody předzpracování byly aplikovány i na ostatní série pořízených rentgenových snímků.
Výsledky aplikace lze vidět v~příloze \ref{sec:results}.
Defekt způsobený záměrně špatnou kalibrací detektoru se projevil ve výsledcích pro obě série snímků (obrátek \ref{fig:demo_board} a \ref{fig:usb_to_rs232}).
Defekt v~podobě vodorovné čáry pixelů je patrný i na výsledných snímcích.
V~případě výsledků Debevecovy metody defekt zůstal patrný v~původně přeexponované oblasti s~přímím dopadem záření.
Na zvýšení dynamického rozsahu například v~oblasti desky plošného spoje obrázku \ref{fig:demo_board} však defekt neměl vliv.
V~případě aplikace metody AAR byl defekt mírně potlačen díky tomu, že při posunu rentgenovaného objektu ve scéně, se defekt nacházel pokaždé v~jiné oblasti snímku.
Po zarovnání snímků a jejich zprůměrování byl defekt patrný pouze v~podobě světlých vodorovných čar.
Lze předpokládat, že čím větší bude počet průměrovaných snímků, tím méně bude defekt patrný.